#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<unistd.h>

#include "MC10.h"

/*
//filetypes
#define BASIC 0x00
#define DATA 0x01
#define ML 0x02

//datatypes
#define BINARY 0x00
#define ASCII 0xff

//gap types
#define CONTINUOUS 0x00
#define GAPS 0xff
*/

//char filename[9];
//int filetype;
//int datatype;
//int gapflag;
//unsigned short startAddress, loadAddress;

int GetLeader(FILE *in);
int GetEOFBlock(FILE *in);
int DisplayFileStats(char *filename, unsigned char filetype, unsigned char datatype, unsigned char gapflag,
						unsigned short startAddress, unsigned short loadAddress);
int GetFileNameBlock(FILE *in, char *filename, unsigned char *filetype, unsigned char *datatype, 
						unsigned char *gapflag, unsigned short *startAddress, unsigned short *loadAddress);

unsigned tt16bits MC10base::GetDataBlocks(FILE *in, unsigned short startAddress, unsigned short loadAddress, char writeflag)
{
	unsigned blocklength, blocktype;
	unsigned char tmp1, tmp2, checksum;
	unsigned short cnt=0;
	int x;
	fpos_t pos;

	if(feof(in))
		return 0;
	
	while(!feof(in))
	{
		checksum=0;
		
		//get header
		fread(&tmp1,1,1,in);
		fread(&tmp2,1,1,in);

	//	if( (tmp1!=0x55)||(tmp2!=0x3c) )
	
		//NOTE: THIS IS A HACK TO ALLOW THE READER TO BE FLEXIBLE
		//		tmp1 SHOULD BE 0x55, BUT WE'LL IGNORE IT FOR NOW
		if(tmp2!=0x3c)
		{
			printf("GetDataBlocks: I didn't find the header $553c, but read $%.2x%.2x instead, exiting\n",tmp1,tmp2);
			return 0;
		}//if
	
		//get block type
		fread(&tmp1,1,1,in);
		blocktype=tmp1;
		checksum+=blocktype;

		//if the block type is eof, rewind 3 bytes and exit
		if(tmp1==0xff)
		{
//			if( (fgetpos(in,&pos))||(pos-=3,fsetpos(in,&pos)) )
			if( (fgetpos(in,&pos)) || (fseek(in, -3, SEEK_CUR)!=0) )
			{
				printf("Couldn't rewind 3 bytes! Exiting\n");
				return 0;
			}//if

			break;
		}//if
	
		if(tmp1!=0x01)
		{
			printf("Instead of 0x01 I recieved 0x%.2x as the filename block type, exiting\n",tmp1);
			return 0;
		}//if

		//get block length
		fread(&tmp1,1,1,in);
		blocklength=tmp1;
		checksum+=blocklength;

		//get data
		for(x=0;x<blocklength;x++)
		{
			fread(&tmp1,1,1,in);
			checksum+=tmp1;

			cnt++;
			if(writeflag)
				Memory[startAddress++] = tmp1;

		}//for

		//get and verify checksum
		fread(&tmp1,1,1,in);
		if(checksum!=tmp1)
			printf("Block: Checksum was read as %.2x but calculated to be %.2x, there are likely errors\n",tmp1,checksum);

		//NOTE: IS THIS WHERE GAPS ARE READ?

		/*
		//burn the end of block id
		fread(&tmp1,1,1,in);
		if(tmp1!=0x55)
		{
			printf("I expected an end-of-block ID of 0x55 but read 0x%.2x instead, exiting\n",tmp1);
			exit(1);
		}//if
		*/

		//burn any 0x55's, and backup to the start of the next header
		tmp1=0x55;
		while(tmp1==0x55)
			fread(&tmp1,1,1,in);

		if(feof(in))
			break;

		if( (fgetpos(in,&pos))||(fseek(in,-2,SEEK_CUR)) )
		{
			printf("Couldn't rewind 2 byte! Exiting\n");
			return 0;
		}//if
	}//while
	
	return cnt;
}//GetDataBlocks

int GetEOFBlock(FILE *in)
{
	unsigned char tmp1,tmp2;
	fpos_t pos;

	if(feof(in))
		return -1;

	//get header
	fread(&tmp1,1,1,in);
	fread(&tmp2,1,1,in);

	//	if( (tmp1!=0x55)||(tmp2!=0x3c) )
	
	//NOTE: THIS IS A HACK TO ALLOW THE READER TO BE FLEXIBLE
	//		tmp1 SHOULD BE 0x55, BUT WE'LL IGNORE IT FOR NOW
	if(tmp2!=0x3c)
	{
		printf("EOFBlock: I didn't find the header $553c, but read $%.2x%.2x instead, exiting\n",tmp1,tmp2);
		return -1;
	}//if
	
	//get block type
	fread(&tmp1,1,1,in);
	if(tmp1!=0xff)
	{
		printf("Instead of 0xff I recieved 0x%.2x as the filename block type, exiting\n",tmp1);
		return -1;
	}//if

	//get block length
	fread(&tmp1,1,1,in);
	if(tmp1!=0x00)
	{
		printf("Instead of 0x00 I recieved 0x%.2x as the filename block length, exiting\n",tmp1);
		return -1;
	}//if

	//read in and verify checksum
	fread(&tmp1,1,1,in);
	if(tmp1!=0xff)
		printf("EOF: I was expecting to get a checksum byte of 0xff but instead read in %.2x, could be errors\n",tmp1);

	/*
	//burn the end of block id
	fread(&tmp1,1,1,in);
	if(tmp1!=0x55)
	{
		printf("I expected a end-of-block ID of 0x55 but read 0x%.2x instead, exiting\n",tmp1);
		exit(1);
	}//if
	*/

	//burn any 0x55's, and backup to the start of the next header
	tmp1=0x55;
	while(tmp1==0x55)
	{
		fread(&tmp1,1,1,in);
		if(feof(in))
			return -1;
	}//while

	if( (fgetpos(in,&pos))||(fseek(in,-2,SEEK_CUR)) )
	{
		printf("Couldn't rewind 2 byte! Exiting\n");
		return -1;
	}//if

	return 0;
}//GetEOFBlock

int GetFileNameBlock(FILE *in, char *filename, unsigned char *filetype, unsigned char *datatype, 
						unsigned char *gapflag, unsigned short *startAddress, unsigned short *loadAddress)
{
	unsigned char tmp1, tmp2, checksum=0;
	fpos_t pos;

	if(feof(in))
		return -1;
		
	//get header
	fread(&tmp1,1,1,in);
	fread(&tmp2,1,1,in);

	//	if( (tmp1!=0x55)||(tmp2!=0x3c) )
	
	//NOTE: THIS IS A HACK TO ALLOW THE READER TO BE FLEXIBLE
	//		tmp1 SHOULD BE 0x55, BUT WE'LL IGNORE IT FOR NOW
	if(tmp2!=0x3c)
	{
		printf("FileBlock: I didn't find the header $553c, but read $%.2x%.2x instead, exiting\n",tmp1,tmp2);
		return -1;
	}//if
	
	//get block type
	fread(&tmp1,1,1,in);
	if(tmp1!=0x00)
	{
		printf("Instead of 0x00 I recieved 0x%.2x as the filename block type, exiting\n",tmp1);
		return -1;
	}//if

	//get block length
	fread(&tmp1,1,1,in);
	if(tmp1!=0x0f)
	{
		printf("Instead of 0x0f I recieved 0x%.2x as the filename block length, exiting\n",tmp1);
		return -1;
	}//if

	//get filename
	for(tmp2=0;tmp2<8;tmp2++)
	{
		fread(&tmp1,1,1,in);
		filename[tmp2]=tmp1;
		if(isspace(tmp1))
			filename[tmp2]='\0';
		checksum+=tmp1;
	}//for

	//get file type
	fread(&tmp1,1,1,in);
	*filetype=tmp1;
	checksum+=tmp1;

	//get data type
	fread(&tmp1,1,1,in);
	*datatype=tmp1;
	checksum+=tmp1;

	//get gap flag
	fread(&tmp1,1,1,in);
	*gapflag=tmp1;
	checksum+=tmp1;
	
	//get start address
	fread(&tmp1,1,1,in);
	fread(&tmp2,1,1,in);
	*startAddress=(tmp1<<8)+tmp2;
	checksum+=tmp1;
	checksum+=tmp2;

	//get load address
	fread(&tmp1,1,1,in);
	fread(&tmp2,1,1,in);
	*loadAddress=(tmp1<<8)+tmp2;
	checksum+=tmp1;
	checksum+=tmp2;

	//verify checksum
	checksum+=0x0f; //sum of data, block type=0x00, and block length=0x0f
	fread(&tmp1,1,1,in);

	if(checksum!=tmp1)
		printf("Checksum was read as %.2x but calculated as %.2x, there are likely errors\n",tmp1,checksum);

	/*
	//burn the end of block id
	fread(&tmp1,1,1,in);
	if(tmp1!=0x55)
	{
		printf("I expected a end-of-block ID of 0x55 but read 0x%.2x instead, exiting\n",tmp1);
		exit(1);
	}//if
	*/

	//burn any 0x55's, and backup to the start of the next header
	tmp1=0x55;
	while(tmp1==0x55)
		fread(&tmp1,1,1,in);

	if(feof(in))
		return -1;

	if( (fgetpos(in,&pos))||(fseek(in,-2,SEEK_CUR)) )
	{
		printf("Couldn't rewind 2 bytes! Exiting\n");
		return -1;
	}//if

	if(DisplayFileStats(filename,*filetype,*datatype,*gapflag,*startAddress,*loadAddress))
		return -1;
		
	return 0;
}//GetFileNameBlock

int GetLeader(FILE *in)
{
	int x=-2,xpos=2;
	unsigned char tmpc=0x55;
	fpos_t pos;

	if(feof(in))
		return -1;

	while(tmpc==0x55)
	{
		x++;
		fread(&tmpc,1,1,in);

		if(feof(in))
			return -1;
	}//while

	//put back two of spots since it's part of the next block's header
	if(x==-1)
		xpos=1;

	if( (fgetpos(in,&pos))||(fseek(in,-xpos,SEEK_CUR)) )
	{
		printf("Couldn't rewind %d bytes! Exiting\n",xpos);
		return -1;
	}//if

//	if(x<128)
//		printf("I was expecting 128 bytes for the leader but instead only read in %d. Could be a problem.\n",x);
	
	return 0;
}//GetLeader

void MC10base::ReadCassette(void)
{
	char filename[9],*tmpname=(char *)malloc(9),cassette[50], writeflag=0;
	unsigned char filetype, datatype, gapflag;
	unsigned short startAddress, loadAddress, totalBytes;
	int x;
	FILE *in;

	printf("Enter in cassette file to read from: ");
	gets(cassette);
//	gets(cassette);

	printf("Enter in filename to locate: ");
	gets(filename);
	for(x=0;x<8;x++)
		filename[x]=toupper(filename[x]);

	if(!(in=fopen(cassette,"rb")))
	{
		printf("Couldn't open file '%s' for reading\n",cassette);
		return;
	}//if

	filename[8]='\0';
	tmpname[8]='\0';

/*	
	//first retrieve the filename from memory
	for(x=0;x<8;x++)
	{
		filename[x]=Memory[0x42b5+x]; //BASIC ROM stores filename at $42b5
		if(!isalnum(filename[x]))
			filename[x]='\0';
	}//for
*/	
	//retrieve each file in the cassette image
	while( (writeflag==0) && (!feof(in)))
	{
		printf("\n");
	
		if(GetLeader(in))
			break;
			
		if(GetFileNameBlock(in,tmpname,&filetype,&datatype,&gapflag,&startAddress,&loadAddress))
			break;

		//set the start and load addresses in memory
		if(filetype!=BASIC)
		{
			Memory[0x426a] = (startAddress>>8);
			Memory[0x426b] = (startAddress&0xff);
			Memory[0x426c] = (loadAddress>>8);
			Memory[0x426d] = (loadAddress&0xff);
		}//if

		//if we're loading a basic program, we need to fudge the start and load addresses
		if(filetype==BASIC)
			startAddress = 0x4346;
		
		if(GetLeader(in))
			break;

		printf("Comparing '%s' to '%s': ", filename, tmpname);
		if(!strcmp(filename,tmpname))
		{
			printf("MATCHED\n");
			writeflag=1;
		}
		else
		{
			printf("NOT MATCHED\n");
			writeflag=0;
		}//if
		
		if((totalBytes = GetDataBlocks(in,startAddress,loadAddress,writeflag))==0)
			break;

		//update other basic areas if need be..
//NOTE: totalBytes MIGHT BE OFF BY ONE
		if( (filetype==BASIC)&&(writeflag==1) )
		{
			totalBytes+=0x4346;

			Memory[0x93]=0x43;
			Memory[0x94]=0x46;
			Memory[0x95]=totalBytes>>8;
			Memory[0x96]=totalBytes&0xff;
		}//if
		
		if(GetEOFBlock(in))
			break;
	}//while

}//ReadCassette

int DisplayFileStats(char *filename, unsigned char filetype, unsigned char datatype, unsigned char gapflag,
						unsigned short startAddress, unsigned short loadAddress)
{
	printf("Filename is: '%s'\n",filename);

	printf("File type is: ");
	switch(filetype)
	{
		case BASIC:
			printf("BASIC\n");
			break;
		case DATA:
			printf("DATA\n");
			break;
		case ML:
			printf("ML\n");
			break;
		default:
			printf("UNKNOWN (%x) - exiting\n",filetype);
			return -1;
	}//switch

	printf("Data type is: ");
	switch(datatype)
	{
		case BINARY:
			printf("BINARY\n");
			break;
		case ASCII:
			printf("ASCII\n");
			break;
		default:
			printf("UNKNOWN (%x) - exiting\n",datatype);
			return -1;
	}//switch

	printf("Gap flag is: ");
	switch(gapflag)
	{
		case GAPS:
			printf("GAPS\n");
			break;
		case CONTINUOUS:
			printf("CONTINUOUS\n");
			break;
		default:
			printf("UNKNOWN (%x) - exiting\n",gapflag);
			return -1;
	}//switch

	printf("Start address is: %.4x\n",startAddress);
	printf("Load address is: %.4x\n",loadAddress);

	return 0;
}//DisplayFileStats


