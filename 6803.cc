#include<stdio.h>
#include"6803.h"
#include"ComputerBase.h"

extern ComputerBase *Computer;

int x6803base::Carry_Flag;
int x6803base::Overflow_Flag;
int x6803base::Zero_Flag;
int x6803base::Negative_Flag;
int x6803base::Interrupt_Flag;
int x6803base::HalfCarry_Flag;

//the registers
unsigned tt8bits x6803base::Accumulator[2];
unsigned tt16bits *x6803base::Accumulator_D;
unsigned tt16bits x6803base::Index_Pointer;
unsigned tt16bits x6803base::Stack_Pointer;
unsigned tt16bits x6803base::Program_Counter;

//To make things faster..
//unsigned tt8bits scratch;
//unsigned tt16bits scratch16;
//unsigned tt32bits scratch32;



//define a constructor
x6803base::x6803base(int mode, int op)
{
  Mode = mode;
  opcode = op;

  //it might get called billions of times, but at least it's isolated
  Accumulator_D = (unsigned tt16bits *)&Accumulator[0];
  Program_Counter = 0xffff;

  //now the internal register stuff..
  //set Port 2 Data Register to 0110 0011
//  Computer->SetRam(0x03,0xff); //was 99
  
  //reset the counter
//  Computer->SetRam(0x09,0);
//  Computer->SetRam(0x0a,0);
  
  //reset the Timer Control Register
//  Computer->SetRam(0x08,0);
  
  //reset the RAM Control Register
//  Computer->SetRam(0x14,0xc0);
  
  //reset Output Compare Register
//  Computer->SetRam(0x0b,0xff);
//  Computer->SetRam(0x0c,0xff);
  
  //reset the keyboard input
//  Computer->SetRam(0xbfff,0xff); //0xff indicates no key pressed -- see keyboard description in notes

}//constructor

/*
inline unsigned tt8bits x6803base::FlagsToVariable(void)
{
	scratch = Carry_Flag*0x01 + Overflow_Flag*0x02 + Zero_Flag*0x04 + Negative_Flag*0x08 +
         Interrupt_Flag*0x10 + HalfCarry_Flag*0x20;

	return scratch;
}//FlagsToVariable

inline void x6803base::VariableToFlags(unsigned tt8bits CCR)
{
  Carry_Flag = (CCR & 0x01);
  Overflow_Flag = (CCR & 0x02) >> 1;
  Zero_Flag = (CCR & 0x04) >> 2;
  Negative_Flag = (CCR & 0x08) >> 3;
  Interrupt_Flag = (CCR & 0x10) >> 4;
  HalfCarry_Flag = (CCR & 0x20) >> 5;
}//VariableToFlags

inline unsigned tt8bits x6803base::Fetch(void)
{
	scratch = FetchMemory(Program_Counter);
	
	Program_Counter++;

	return scratch;
//	return FetchMemory(Program_Counter-1);
}//Fetch

inline unsigned tt8bits x6803base::FetchMemory(unsigned tt16bits pos)
{
//if((Mode==Index)||(Mode==Extended))
//	LastPosition=pos;

  return Computer->FetchMemory(pos);
}//FetchMemory

inline unsigned tt16bits x6803base::FetchAddress(void)
{
  switch(Mode)
  {
    case Direct:
      return Fetch();
    case Index:
      return Fetch() + Index_Pointer;
    case Extended:
      return (Fetch() << 8) + Fetch();
    default:
      printf("Tried fetching FetchAddress with mode %d\n",Mode);
      return 0;
  }//switch
}//FetchAddress

inline unsigned tt8bits x6803base::FetchData(void)
{
	switch(Mode)
	{
		case Immediate:
	    case Relative:
	      	return Fetch();
	    case Index:
	      	return FetchMemory(Fetch() + Index_Pointer);
	    case Direct:
			return FetchMemory(Fetch());
	    case Extended:
    		return FetchMemory((Fetch()<<8) + Fetch());
	    default:
		    printf("Invalid mode %d in function FetchData8 called from %s!!!",Mode, mnemonic);
		    return 0;
	}//switch
}//FetchData

inline unsigned tt16bits x6803base::FetchData16(void)
{
  switch(Mode)
  {
    case Immediate:
      return (Fetch()<<8) + Fetch();
      break;
    case Index:
      scratch16 = Fetch() + Index_Pointer;
      break;
    case Direct:
      scratch16 = Fetch();
      break;
    case Extended:
      scratch16 = (Fetch() << 8) + Fetch();
      break;
    default:
      printf("Invalid mode %d in function FetchData8!!!\n",Mode);
      return 0;         
  }//switch

  return (FetchMemory(scratch16) << 8) + FetchMemory(scratch16+1);
}//fetchData16

inline void x6803base::SetMemory(unsigned tt16bits pos, unsigned tt8bits val)
{
  Computer->SetMemory(pos,val);
}//SetMemory

inline void x6803base::SetMemory16(unsigned tt16bits pos, unsigned tt16bits val)
{
  SetMemory(pos,val >> 8);
  SetMemory(pos+1,val & 0xff);
}//SetMemory16

inline void x6803base::SetLastRead(unsigned tt8bits val)
{

  unsigned tt16bits Position;

  switch(Mode)
  {
    case Direct:
      Position = FetchMemory(Program_Counter - 1);
      break;
    case Index:
      Position = FetchMemory(Program_Counter - 1) + Index_Pointer;
      break;
    case Extended:
      Position = (FetchMemory(Program_Counter - 2) << 8) + FetchMemory(Program_Counter - 1);
      break;
    default:
      printf("Tried to set last read with mode %d\n",Mode);
      Position = 0;
  }//switch

	SetMemory(Position,val);

//Position = LastPosition;


//  SetMemory(LastPosition, val);
}//SetLastRead
*/

/*
inline void x6803base::SetLastRead16(unsigned tt16bits val)
{
  unsigned tt16bits Position;

  switch(Mode)
  {
    case Direct:
      Position = FetchMemory(Program_Counter - 1);
      break;
    case Index:
      Position = FetchMemory(Program_Counter - 1) + Index_Pointer;
      break;
    case Extended:
      Position = (FetchMemory(Program_Counter - 2) << 8) + FetchMemory(Program_Counter - 1);
      break;
    default:
      printf("Tried to set last read with mode %d\n",Mode);
      Position = 0;
  }//switch

  SetMemory16(Position,val);
}//SetLastRead16
*/

/*
//stack functions (to reduce code errors)
inline void x6803base::PushStack(unsigned tt8bits val)
{
  SetMemory(Stack_Pointer,val);
  Stack_Pointer--;
}//PushStack

inline void x6803base::PushStack16(unsigned tt16bits val)
{
  SetMemory16(Stack_Pointer-1,val);
  Stack_Pointer-=2;  
}//PushStack16

inline unsigned tt8bits x6803base::PopStack(void)
{
  Stack_Pointer++;
  return FetchMemory(Stack_Pointer);
}//PopStack

inline unsigned tt16bits x6803base::PopStack16(void)
{
  Stack_Pointer+=2;
  return FetchMemory(Stack_Pointer) + (FetchMemory(Stack_Pointer-1)<<8);
}//PopStack16
*/

//ALU parts (Should these be defines?)
/*
inline unsigned tt8bits x6803base::Add(unsigned tt8bits first, unsigned tt8bits second)
{
  scratch16 = first + second;
  SET8_HNZVC(first, second,scratch16);

  return (scratch16&0xff);
}//Add

inline unsigned tt8bits x6803base::AddCarry(unsigned tt8bits first, unsigned tt8bits second)
{
	scratch16 = first + second + Carry_Flag;
	SET8_HNZVC(first,second,scratch16);

	return (scratch16&0xff);  
}//AddCarry

inline unsigned tt16bits x6803base::Add16(unsigned tt16bits first, unsigned tt16bits second)
{
  scratch32 = first + second;

  SET16_NZVC(first,second,scratch32);

  return (scratch32&0xffff);
}//Add16

inline unsigned tt8bits x6803base::Subtract(unsigned tt8bits first, unsigned tt8bits second)
{
	scratch16 = first-second;
	SET8_NZVC(first,second,scratch16);

  return (scratch16 & 0xff);
}//Subtract

inline unsigned tt8bits x6803base::SubtractCarry(unsigned tt8bits first, unsigned tt8bits second)
{
	scratch16 = first - second - Carry_Flag;
	SET8_NZVC(first,second,scratch16);

	return (scratch16 & 0xff);
}//SubtractCarry

inline unsigned tt16bits x6803base::Subtract16(unsigned tt16bits first, unsigned tt16bits second)
{
	scratch32 = first - second;
  
	SET16_NZVC(first,second,scratch32);

	return (scratch32 & 0xffff);
}//Subtract16
 
//Now the logical operations:
inline unsigned tt8bits x6803base::AND(unsigned tt8bits first, unsigned tt8bits second)
{
  unsigned tt8bits result = first & second;

  SET8_NZ(result);
  Overflow_Flag=0;

  return result;
}//AND

inline unsigned tt8bits x6803base::Complement(unsigned tt8bits first)
{
  unsigned tt8bits result = first ^ 0xff;

  SET8_NZ(result);
  Overflow_Flag = 0;
  Carry_Flag = 1;

  return result;
}//Complement

inline unsigned tt8bits x6803base::EOR(unsigned tt8bits first, unsigned tt8bits second)
{
  unsigned tt8bits result = first ^ second;

  SET8_NZ(result);
  Overflow_Flag = 0;

  return result;
}//EOR

inline unsigned tt8bits x6803base::Negate(unsigned tt8bits first)
{
  unsigned tt8bits result = (first ^ 0xff) + 1;

  SET8_NZ(result);

  Overflow_Flag = (result == 0x80);
  Carry_Flag = (result == 0x00);

  return result;

//  return Subtract(0,first);
}//Negate

inline unsigned tt8bits x6803base::OR(unsigned tt8bits first, unsigned tt8bits second)
{
  unsigned tt8bits result = first | second;

  SET8_NZ(result);
  Overflow_Flag = 0;

  return result;
}//OR

inline unsigned tt8bits x6803base::Decrement(unsigned tt8bits first)
{
  //should overflow be set?
  Overflow_Flag = (first == 0x80);

  SET8_NZ(first-1);

  return first-1;
}//Decrement

inline unsigned tt8bits x6803base::Increment(unsigned tt8bits first)
{
  //should overflow be set?
  Overflow_Flag = (first == 0x7f);

  SET8_NZ(first+1);

  return first+1;
}//Increment

//Handle shifting
#define ArithmeticShift 1
#define LogicalShift 2
#define Rotate 3

inline unsigned tt8bits x6803base::ShiftLeft(unsigned tt8bits first, int ShiftMode)
{
  unsigned tt8bits result = (first << 1);

  //we only need to change the result if it's a rotate
  if(ShiftMode==Rotate)
    result += Carry_Flag;

  Carry_Flag = ((first & 0x80) >> 7);

  SET8_NZ(result);
      
  //check for overflow
  //if( ((first&0x80)>>7)^((first&0x40)>>6)) //remember >>7 and >>6 to get the bits..
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;

  return result;
}//ShiftLeft

inline unsigned tt8bits x6803base::ArithmeticShiftLeft(unsigned tt8bits first)
{
  return ShiftLeft(first,ArithmeticShift);
}//ArithmeticShiftLeft

inline unsigned tt8bits x6803base::RotateLeft(unsigned tt8bits first)
{
  return ShiftLeft(first,Rotate);
}//RotateLeft

inline unsigned tt16bits x6803base::ShiftLeft16(unsigned tt16bits first)
{
  unsigned tt16bits result = (first << 1);

  //remember that the only left double shift operation for the 6803 is logical/arithmetic shift

  Carry_Flag = ((first & 0x8000) >> 15);

  SET16_NZ(result);

  //check for overflow
  //if( ((first&0x8000)>>15)^((first&0x4000)>>14))
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;

  return result;
}//ShiftLeft16

inline unsigned tt16bits x6803base::ShiftRight16(unsigned tt16bits first)
{
  unsigned tt16bits result = first >> 1;

  //remember that the only double shift operation for the 6803 is logical/arithmetic shift

  Carry_Flag = (first & 0x01);

  SET16_NZ(result);

  //check for overflow
  //if( ((first&0x8000)>>15)^((first&0x4000)>>14))
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;

  return result;
}//ShiftRight16

inline unsigned tt8bits x6803base::ShiftRight(unsigned tt8bits first, int ShiftMode)
{
  unsigned tt8bits result = (first >> 1);

  switch(ShiftMode)
  {
    case ArithmeticShift:
      result |= (first&0x80);
      break;          
    case Rotate:
      result |= (Carry_Flag << 7);
      break;
    case LogicalShift:
      break;
	default:
		printf("Bad shift!\n");
  }//switch

  Carry_Flag = (first & 0x01);

  //check for overflow
  //if( ((first&0x80)>>7)^((first&0x40)>>6)) //remember >>7 and >>6 to get the bits..
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  SET8_NZ(result);

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;
 
  return result;
}//ShiftRight8
  
inline unsigned tt8bits x6803base::ArithmeticShiftRight(unsigned tt8bits first)
{
  return ShiftRight(first,ArithmeticShift);
}//ArithmeticShiftRight8

inline unsigned tt8bits x6803base::LogicalShiftRight(unsigned tt8bits first)
{
  return ShiftRight(first,LogicalShift);
}//LogicalShiftRight8

inline unsigned tt8bits x6803base::RotateRight(unsigned tt8bits first)
{
  return ShiftRight(first,Rotate);
}//RotateRight8
*/
//********************************************************************************//





