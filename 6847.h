//6847 emulator
#ifndef __x6847_H
#define __x6847_H

#include "ComputerBase.h"

#define x8x12Block unsigned char[12];

//Define colours
#define Black		1
#define Green		2
#define Yellow		3
#define Blue		4
#define Red			5
#define Buff		6
#define Cyan		7
#define Magenta		8
#define Orange		9

//Define video modes
#define SG4 1
#define SG6 2
#define CG1 3
#define RG1 4
#define CG2 5
#define RG2 6
#define CG3 7
#define RG3 8
#define CG6 9
#define RG6 10

class x6847base
{
	public:
		virtual void Init(void) = 0; //Create the 256x192 window
		virtual void Quit(void) = 0; //Close up everything
//		virtual void DrawBlock(x8x12Block, int, int);
//		virtual void DrawPixel(int, int, int);
		virtual void UpdateDisplay(unsigned tt16bits pos, unsigned tt8bits value) = 0;
		virtual void UpdateChip(unsigned tt8bits val) = 0;
		virtual int PollKey() = 0;

//		x6847base(){}; //Empty constructor

//	protected:
//		static int VideoMode;
//		static int bgColour;
//		static int fgColour;
};//x6847base


#endif
