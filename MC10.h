#ifndef __MC10_H
#define __MC10_H

#include<ctype.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "ComputerBase.h"
#include "6803.h"
#include "6847X11.h"

#define MEMLENGTH 49152
#define ROMSIZE 8192
#define CPUSPEED 0.89 //speed in Mhz

//4k or 16k?
#define MAXRAM 0x8fff

#define WRITEHOOK 0x03
#define READHOOK 0x02


//filetypes
#define BASIC 0x00
#define DATA 0x01
#define ML 0x02

//datatypes
#define BINARY 0x00
#define ASCII 0xff

//gap types
#define CONTINUOUS 0x00
#define GAPS 0xff

#define LOADCAS 0xc0 //F3
#define SAVECAS 0xbf //F2
#define CONVERTCOCO 0xc1 //F4
#define STARTDEBUG 0xc2 //F5

//unsigned short last_PC;

//********************************************************************************//

class MC10base : ComputerBase
{
  public:
    int Debug;
    int DebugPoint;
	int WatchPoint;
	int WatchValue;

	int ShiftSet;
	unsigned short last_PC;

	//private cassette handler stuff
	int lastkey;
	void ReadCassette(void);
	unsigned tt16bits GetDataBlocks(FILE *,unsigned short,unsigned short,char);
	void WriteCassette(int,int,unsigned short,unsigned short,unsigned short);
	void CoCoTokenConvert(void);
	
  public:  
  	SysDep6847 *x6847Chip;
    //stuff for the 6803 chip:
    x6803base *OpCodes[256];
    unsigned char reg[2]; //accumulator A and B
    
	unsigned tt8bits KeyColumn;
    unsigned char Memory[MEMLENGTH]; //16k of ram
    unsigned char ROM[ROMSIZE];
    struct timeb *tp; //for timing purposes    
    
    void init(void);
	void CheckKey(void);

	inline void HandleInvalidOpCode(unsigned tt8bits opcode)
	{
		switch(opcode)
		{
			case READHOOK:
				ReadCassette();
				break;
			case WRITEHOOK:

			default:
				printf("Tried to execute invalid opcode %.2x, exiting\n",opcode);
				exit(1);
		}//switch
	}//HandleInvalidOpCode
	


    inline unsigned tt8bits isLegalMemory(unsigned tt16bits address)
    //Determine if given address is valid
    {return (                    address<=0x001f ) || /* i/o       */
            ( address>=0x0080 && address<=0x00ff ) || /* int ram   */
            ( address>=0x4000 && address<=0x4fff ) || /* video ram */
            ( address>=0x5000 && address<=0x8fff ) || /* 16K exp   */
            ( address>=0xc000                    );
    }
	
    inline unsigned tt8bits FetchMemory(unsigned tt16bits address)
    //Fetch the byte at the given address
    {

      //was it chip or internal ram or external ram?
      if( ((address>=0x0080)&&(address<=0x00ff)) ||
          ((address>=0x4200)&&(address<=MAXRAM)) )
        return Memory[address];

		//is it external ram?
//!!!!!!!!!! < or <= 0x8fff?
//	  if( (address>0x5000) && (address<=0x9fff) )
//	  	return Memory[address];
  
      //was it the BASIC ROM?      
      if( (address>=0xe000) && (address<=0xffff) )
        return ROM[address-0xe000];
     
      //was it the EXTERNAL ROM EXPANSION?
      //for this we'll just mirror the BASIC ROM..   
      if( (address>=0xc000) && (address<=0xdfff) )
        return ROM[address-0xc000];
        
      //is it the video ram?
      if( (address>=0x4000) && (address<0x4200) )
        return Memory[address];

      //is it the keybord input?
      if(address==0xbfff)
       return  (~Memory[0x02]&0x01 ? x6847Chip->Port1[0] : 0xff) &
      	       (~Memory[0x02]&0x02 ? x6847Chip->Port1[1] : 0xff) &
      	       (~Memory[0x02]&0x04 ? x6847Chip->Port1[2] : 0xff) &
      	       (~Memory[0x02]&0x08 ? x6847Chip->Port1[3] : 0xff) &
      	       (~Memory[0x02]&0x10 ? x6847Chip->Port1[4] : 0xff) &
      	       (~Memory[0x02]&0x20 ? x6847Chip->Port1[5] : 0xff) &
      	       (~Memory[0x02]&0x40 ? x6847Chip->Port1[6] : 0xff) &
      	       (~Memory[0x02]&0x80 ? x6847Chip->Port1[7] : 0xff);

      //is it an internal register?
      if(address<=0x1f)
        switch(address)
        {
          case 0x00: //Port 1 Data Direction Register
          case 0x01: //Port 2 Data Direction Register
            return Memory[address];
          
          case 0x02: //Port 1 Data Register (Keyboard Scan Strobe - Columns)
printf("02 is %x\n",Memory[address]);		  
            return Memory[address];
          
          case 0x03: //Port 2 Data Register (Printer/Cassette and Keyboard)
//printf("03 is %x\n",Memory[address]);

			return (~Memory[0x02]&0x01 ? x6847Chip->Port2[0] : 0xff) &
			       (~Memory[0x02]&0x02 ? x6847Chip->Port2[1] : 0xff) &
			       (~Memory[0x02]&0x04 ? x6847Chip->Port2[2] : 0xff) &
			       (~Memory[0x02]&0x08 ? x6847Chip->Port2[3] : 0xff) &
			       (~Memory[0x02]&0x10 ? x6847Chip->Port2[4] : 0xff) &
			       (~Memory[0x02]&0x20 ? x6847Chip->Port2[5] : 0xff) &
			       (~Memory[0x02]&0x40 ? x6847Chip->Port2[6] : 0xff) &
			       (~Memory[0x02]&0x80 ? x6847Chip->Port2[7] : 0xff);

          case 0x04: //External Memory
          case 0x05: //External Memory
          case 0x06: //External Memory
          case 0x07: //External Memory
		  	return Memory[address];
          case 0x08: //Timer Control and Status Register           
          case 0x09: //Counter (High byte)
          case 0x0a: //Counter (Low byte)
			printf("Access counter\n");
          case 0x0b: //Output Compare Register (High byte)
          case 0x0c: //Output Compare Register (Low byte)          
          case 0x0d: //Input Capture Register (High byte)
          case 0x0e: //Input Capture Register (Low byte)            
          case 0x0f: //External Memory          
            return Memory[address];

          
          case 0x10: //Rate and Mode Control Register
            break;

          case 0x11: //Transmit/Recieve Control and Status Register
            return Memory[address];
            
          case 0x12: //Recieve Data Register
            return Memory[0x12];
          
          case 0x13: //Transmit Data Register
			return Memory[0x13];

          case 0x14: //RAM Control Register
            return Memory[0x14];          
        
          default:
          
//return 0;
          
 //           printf("(%x) Attempted to read to reserved internal register area %x.\n",OpCodes[0x00]->Program_Counter-1,address);
 			return 0x00;

            break;
        }//switch


//return 255;
        
//      printf("\n(%x) Tried to read an invalid area of memory at %xh (%ld)\n",
//	  		OpCodes[0x00]->Program_Counter-1,address,address);
//      ShowInstruction(last_PC);
//      ShowRegisters();
//      Debug=1;
//	  printf("\n");
	  return 0x00; //is this correct?
     
      printf("Dumping core :)\n");
      fflush(stdout);
      strcpy(NULL,NULL);
      
      printf("Exit 1\n");
      exit(1);
      
    };//FetchMemory

    inline unsigned tt8bits FetchOpCode(void)
    {
      OpCodes[0x00]->Program_Counter++;      
      return FetchMemory(OpCodes[0x00]->Program_Counter-1);
    }//FetchOpCode
    
    inline void SetMemory(unsigned tt16bits address, unsigned tt8bits value)
    {

      //is it writable ram?
//!!!!!!!!!!!!!! < or <= 0x8fff
      if( ((address>=0x0080)&&(address<0x0100)) ||
          ((address>=0x4200)&&(address<=MAXRAM)) ) 
      {
        Memory[address] = value;
        return;
      }//if
      
      //is it VDG and SOUND O/P?
      if(address==0xbfff)
      {
        //printf("(%x) Tried to write to VDG and SOUND O/P with %d (%c)\n",
        //			OpCodes[0x00]->Program_Counter-1,(unsigned)value,value);
        //ShowRegisters();

		x6847Chip->UpdateChip(value);
        return;
//        exit(1);
      }//if
      
      //is it video ram?
      if( (address>=0x4000)&&(address<0x4200) )
      {
        Memory[address] = value;

//printf("Tried to write %x (%d) at %x\n",value,value,address);

		x6847Chip->UpdateDisplay(address-0x4000,value);
		return;

      }//if
      
      //is it an internal register?
      if(address<=0x1f)
      {
        switch(address)
        {
          case 0x00: //Port 1 direction register
          case 0x01: //Port 2 direction register
            Memory[address] = value;
            return;
          case 0x02: //Port 1 Data Register
            Memory[address] = value;
			return;
          case 0x03: //Port 2 Data Register
			Memory[address]=0x03;
            return;

          //external memory
          case 0x04:
          case 0x05:
          case 0x06:
          case 0x07:
            Memory[address] = value;
            return;
          
          case 0x08: 
            if(value > 31) //only bits 0-4 are writable
              break;
              
            Memory[0x08] = (Memory[0x08] & 0xe0) + value;
            return;
            
          case 0x09:
            Memory[0x09] = 0xff;
            Memory[0x0a] = 0xf8;
            return;
            
          case 0x0a:
          case 0x0b:
          case 0x0c:
          case 0x0d:
          case 0x0e:
            break;
            
          case 0x0f: //external memory
            Memory[0x0f] = value;
            return;
            
          case 0x10:
          case 0x11:
          case 0x12:
          case 0x13:
          case 0x14:
          default:
           printf("(%x) Attempted to write to reserved internal register area %x.\n",
                     OpCodes[0x00]->Program_Counter-1,address);
        }//switch
      }//if
        
      //printf("\n(%x) Tried to write to invalid area of memory at %x (%ld) with %d (%c)\n",
      //       OpCodes[0x00]->Program_Counter-1,address,address,value,value);
      //ShowInstruction(last_PC);  
      //ShowRegisters();  
//      Debug=1;
      //printf("\n");          
    }; //SetMemory

    void SetRam(unsigned tt16bits address, unsigned tt8bits value)
    {
      Memory[address] = value;
    };//SetRam
        
    void ShowInstruction(unsigned short start_address)
    {unsigned char opcode;
     opcode=FetchMemory(start_address);
     printf("%04hX: %5s",
            start_address,
            OpCodes[opcode]->mnemonic);
     if (OpCodes[opcode]->Mode == Relative)
      printf(" %04hX",
             start_address + 2 +
             (signed char) 
             FetchMemory(start_address+1));
     else if (OpCodes[opcode]->Mode == Direct)
      printf("   %02X",
             FetchMemory(start_address+1));
     else if (OpCodes[opcode]->Mode == Index)
      printf(" %02X,X",
             FetchMemory(start_address+1));
     else if (OpCodes[opcode]->Mode == Extended)
      printf(" %02X%02X",
             FetchMemory(start_address+1),
             FetchMemory(start_address+2));
     else if (opcode==0x83 ||
              opcode==0xc3 ||
              opcode==0x8c ||
              opcode==0xcc ||
              opcode==0x8e ||
              opcode==0xce)
      printf("#%02X%02X",
              FetchMemory(start_address+1),
              FetchMemory(start_address+2));
     else if (OpCodes[opcode]->Mode == Immediate)
      printf("  #%02X",
             FetchMemory(start_address+1));
	 else
	  printf("     ");
      
     putchar(' '); 
 	}
  
    void ShowRegisters(void)
    {unsigned char opcode; 
     printf("PC:%04X D:%04X X:%04X S:%04X CC: ",
            OpCodes[0x00]->Program_Counter,
           *OpCodes[0x00]->Accumulator_D,
            OpCodes[0x00]->Index_Pointer,
            OpCodes[0x00]->Stack_Pointer
            );
     putchar(OpCodes[0x00]->HalfCarry_Flag ? 'H' : '.');
     putchar(OpCodes[0x00]->Interrupt_Flag ? 'I' : '.');
     putchar(OpCodes[0x00]->Negative_Flag  ? 'N' : '.');
     putchar(OpCodes[0x00]->Zero_Flag      ? 'Z' : '.');
     putchar(OpCodes[0x00]->Overflow_Flag  ? 'V' : '.');
     putchar(OpCodes[0x00]->Carry_Flag     ? 'C' : '.');
     putchar('\n');
    }
    
};//MC10base class


/*
void MC10base::CheckKey(void)
{
	x6847Chip->PollKey();
}//CheckKey
*/

//**************************************************************************//

/*
void MC10base::init(void)
{
	int x;

	x6847Chip = new SysDep6847();
	
	memset(&Memory,0,sizeof(Memory));
		    
	for(x=0;x<255;x++)
	{
		OpCodes[x]=NULL;
	}//for

	//generate opcode table:

	//ABA
	OpCodes[0x1b] = new ABA(Inherent,0x1b);

	//ABX 
	OpCodes[0x3a] = new ABX(Inherent,0x3a);

	//ADCA
	OpCodes[0x89] = new ADCA(Immediate,0x89);
	OpCodes[0x99] = new ADCA(Direct,0x99);
	OpCodes[0xa9] = new ADCA(Index,0xa9);
	OpCodes[0xb9] = new ADCA(Extended,0xb9);

	//ADCB 
	OpCodes[0xc9] = new ADCB(Immediate,0xc9);
	OpCodes[0xd9] = new ADCB(Direct,0xd9);
	OpCodes[0xe9] = new ADCB(Index,0xe9);
	OpCodes[0xf9] = new ADCB(Extended,0xf9);

	//ADDA 
	OpCodes[0x8b] = new ADDA(Immediate,0x8b);
	OpCodes[0x9b] = new ADDA(Direct,0x9b);
	OpCodes[0xab] = new ADDA(Index,0xab);
	OpCodes[0xbb] = new ADDA(Extended,0xbb);

	//ADDB 
	OpCodes[0xcb] = new ADDB(Immediate,0xcb);
	OpCodes[0xdb] = new ADDB(Direct,0xdb);
	OpCodes[0xeb] = new ADDB(Index,0xeb);
	OpCodes[0xfb] = new ADDB(Extended,0xfb);

	//ADDD 
	OpCodes[0xc3] = new ADDD(Immediate,0xc3);
	OpCodes[0xd3] = new ADDD(Direct,0xd3);
	OpCodes[0xe3] = new ADDD(Index,0xe3);
	OpCodes[0xf3] = new ADDD(Extended,0xf3);

	//ANDA 
	OpCodes[0x84] = new ANDA(Immediate,0x84);
	OpCodes[0x94] = new ANDA(Direct,0x94);
	OpCodes[0xa4] = new ANDA(Index,0xa4);
	OpCodes[0xb4] = new ANDA(Extended,0xb4);

	//ANDB 
	OpCodes[0xc4] = new ANDB(Immediate,0xc4);
	OpCodes[0xd4] = new ANDB(Direct,0xd4);
	OpCodes[0xe4] = new ANDB(Index,0xe4);
	OpCodes[0xf4] = new ANDB(Extended,0xf4);

	//ASL 
	OpCodes[0x68] = new ASL(Index,0x68);
	OpCodes[0x78] = new ASL(Extended,0x78);

	//ASLA 
	OpCodes[0x48] = new ASLA(Inherent,0x48);

	//ASLB 
	OpCodes[0x58] = new ASLB(Inherent,0x58);

	//ASLD 
	OpCodes[0x05] = new ASLD(Inherent,0x05);

	//ASR 
	OpCodes[0x67] = new ASR(Index,0x67);
	OpCodes[0x77] = new ASR(Extended,0x77);

	//ASRA 
	OpCodes[0x47] = new ASRA(Inherent,0x47);

	//ASRB 
	OpCodes[0x57] = new ASRB(Inherent,0x57);

	//BRA 
	OpCodes[0x20] = new BRA(Relative,0x20);

	//BRN 
	OpCodes[0x21] = new BRN(Relative,0x21);

	//BCC 
	OpCodes[0x24] = new BCC(Relative,0x24);

	//BCS 
	OpCodes[0x25] = new BCS(Relative,0x25);

	//BEQ	
	OpCodes[0x27] = new BEQ(Relative,0x27);

	//BGE	
	OpCodes[0x2c] = new BGE(Relative,0x2c);

	//BGT	
	OpCodes[0x2e] = new BGT(Relative,0x2e);

	//BHI	
	OpCodes[0x22] = new BHI(Relative,0x22);

	//BLE	
	OpCodes[0x2f] = new BLE(Relative,0x2f);

	//BLS	
	OpCodes[0x23] = new BLS(Relative,0x23);

	//BLT	
	OpCodes[0x2d] = new BLT(Relative,0x2d);

	//BMI	
	OpCodes[0x2b] = new BMI(Relative,0x2b);

	//BNE	
	OpCodes[0x26] = new BNE(Relative,0x26);

	//BVC	
	OpCodes[0x28] = new BVC(Relative,0x28);

	//BVS	
	OpCodes[0x29] = new BVS(Relative,0x29);

	//BPL	
	OpCodes[0x2a] = new BPL(Relative,0x2a);  

	//BSR	
	OpCodes[0x8d] = new BSR(Relative,0x8d);

	//BITA	
	OpCodes[0x85] = new BITA(Immediate,0x85);
	OpCodes[0x95] = new BITA(Direct,0x95);
	OpCodes[0xa5] = new BITA(Index,0xa5);
	OpCodes[0xb5] = new BITA(Extended,0xb5);

	//BITB	
	OpCodes[0xc5] = new BITB(Immediate,0xc5);
	OpCodes[0xd5] = new BITB(Direct,0xd5);
	OpCodes[0xe5] = new BITB(Index,0xe5);
	OpCodes[0xf5] = new BITB(Extended,0xf5);

	//CBA	
	OpCodes[0x11] = new CBA(Inherent,0x11);

	//CLC	
	OpCodes[0x0c] = new CLC(Inherent,0x0c);

	//CLI	
	OpCodes[0x0e] = new CLI(Inherent,0x0e);

	//CLR	
	OpCodes[0x6f] = new CLR(Index,0x6f);
	OpCodes[0x7f] = new CLR(Extended,0x7f);

	//CLRA	
	OpCodes[0x4f] = new CLRA(Inherent,0x4f);

	//CLRB	
	OpCodes[0x5f] = new CLRB(Inherent,0x5f);

	//CLV	
	OpCodes[0x0a] = new CLV(Inherent,0x0a);

	//CMPA	
	OpCodes[0x81] = new CMPA(Immediate,0x81);
	OpCodes[0x91] = new CMPA(Direct,0x91);
	OpCodes[0xa1] = new CMPA(Index,0xa1);
	OpCodes[0xb1] = new CMPA(Extended,0xb1);

	//CMPB	
	OpCodes[0xc1] = new CMPB(Immediate,0xc1);
	OpCodes[0xd1] = new CMPB(Direct,0xd1);
	OpCodes[0xe1] = new CMPB(Index,0xe1);
	OpCodes[0xf1] = new CMPB(Extended,0xf1);

	//COM	
	OpCodes[0x63] = new COM(Index,0x63);
	OpCodes[0x73] = new COM(Extended,0x73);

	//COMA	
	OpCodes[0x43] = new COMA(Inherent,0x43);

	//COMB	
	OpCodes[0x53] = new COMB(Inherent,0x53);

	//CPX	
	OpCodes[0x8c] = new CPX(Immediate,0x8c);
	OpCodes[0x9c] = new CPX(Direct,0x9c);
	OpCodes[0xac] = new CPX(Index,0xac);
	OpCodes[0xbc] = new CPX(Extended,0xbc);

	//DAA	
	OpCodes[0x19] = new DAA(Inherent,0x19);

	//DEC	
	OpCodes[0x6a] = new DEC(Index,0x6a);
	OpCodes[0x7a] = new DEC(Extended,0x7a);

	//DECA	
	OpCodes[0x4a] = new DECA(Inherent,0x4a);

	//DECB	
	OpCodes[0x5a] = new DECB(Inherent,0x5a);

	//DES	
	OpCodes[0x34] = new DES(Inherent,0x34);

	//DEX	
	OpCodes[0x09] = new DEX(Inherent,0x09);

	//EORA	
	OpCodes[0x88] = new EORA(Immediate,0x88);
	OpCodes[0x98] = new EORA(Direct,0x98);
	OpCodes[0xa8] = new EORA(Index,0xa8);
	OpCodes[0xb8] = new EORA(Extended,0xb8);

	//EORB	
	OpCodes[0xc8] = new EORB(Immediate,0xc8);
	OpCodes[0xd8] = new EORB(Direct,0xd8);
	OpCodes[0xe8] = new EORB(Index,0xe8);
	OpCodes[0xf8] = new EORB(Extended,0xf8);

	//INC	
	OpCodes[0x6c] = new INC(Index,0x6c);
	OpCodes[0x7c] = new INC(Extended,0x7c);

	//INCA	
	OpCodes[0x4c] = new INCA(Inherent,0x4c);

	//INCB	
	OpCodes[0x5c] = new INCB(Inherent,0x5c);

	//INS	
	OpCodes[0x31] = new INS(Inherent,0x31);

	//INX	
	OpCodes[0x08] = new INX(Inherent,0x08);

	//JMP	
	OpCodes[0x6e] = new JMP(Index,0x6e);
	OpCodes[0x7e] = new JMP(Extended,0x7e);

	//JSR	
	OpCodes[0x9d] = new JSR(Direct,0x9d);
	OpCodes[0xad] = new JSR(Index,0xad);
	OpCodes[0xbd] = new JSR(Extended,0xbd);

	//LDAA	
	OpCodes[0x86] = new LDAA(Immediate,0x86);
	OpCodes[0x96] = new LDAA(Direct,0x96);
	OpCodes[0xa6] = new LDAA(Index,0xa6);
	OpCodes[0xb6] = new LDAA(Extended,0xb6);

	//LDAB	
	OpCodes[0xc6] = new LDAB(Immediate,0xc6);
	OpCodes[0xd6] = new LDAB(Direct,0xd6);
	OpCodes[0xe6] = new LDAB(Index,0xe6);
	OpCodes[0xf6] = new LDAB(Extended,0xf6);

	//LDD	
	OpCodes[0xcc] = new LDD(Immediate,0xcc);
	OpCodes[0xdc] = new LDD(Direct,0xdc);
	OpCodes[0xec] = new LDD(Index,0xec);
	OpCodes[0xfc] = new LDD(Extended,0xfc);

	//LDS	
	OpCodes[0x8e] = new LDS(Immediate,0x8e);
	OpCodes[0x9e] = new LDS(Direct,0x9e);
	OpCodes[0xae] = new LDS(Index,0xae);
	OpCodes[0xbe] = new LDS(Extended,0xbe);

	//LDX	
	OpCodes[0xce] = new LDX(Immediate,0xce);
	OpCodes[0xde] = new LDX(Direct,0xde);
	OpCodes[0xee] = new LDX(Index,0xee);
	OpCodes[0xfe] = new LDX(Extended,0xfe);

	//LSR	
	OpCodes[0x64] = new LSR(Index,0x64);
	OpCodes[0x74] = new LSR(Extended,0x74);

	//LSRA	
	OpCodes[0x44] = new LSRA(Inherent,0x44);

	//LSRB	
	OpCodes[0x54] = new LSRB(Inherent,0x54);

	//LSRD	
	OpCodes[0x04] = new LSRD(Inherent,0x04);

	//MUL	
	OpCodes[0x3d] = new MUL(Inherent,0x3d);

	//NEG	
	OpCodes[0x60] = new NEG(Index,0x60);
	OpCodes[0x70] = new NEG(Extended,0x70);

	//NEGA	
	OpCodes[0x40] = new NEGA(Inherent,0x40);

	//NEGB	
	OpCodes[0x50] = new NEGB(Inherent,0x50);

	//NOP	
	OpCodes[0x01] = new NOP(Inherent,0x01);

	//ORAA	
	OpCodes[0x8a] = new ORAA(Immediate,0x8a);
	OpCodes[0x9a] = new ORAA(Direct,0x9a);
	OpCodes[0xaa] = new ORAA(Index,0xaa);
	OpCodes[0xba] = new ORAA(Extended,0xba);

	//ORAB	
	OpCodes[0xca] = new ORAB(Immediate,0xca);
	OpCodes[0xda] = new ORAB(Direct,0xda);
	OpCodes[0xea] = new ORAB(Index,0xea);
	OpCodes[0xfa] = new ORAB(Extended,0xfa);

	//PSHA	
	OpCodes[0x36] = new PSHA(Inherent,0x36);

	//PSHB	
	OpCodes[0x37] = new PSHB(Inherent,0x37);  

	//PSHX	
	OpCodes[0x3c] = new PSHX(Inherent,0x3c);

	//PULA	
	OpCodes[0x32] = new PULA(Inherent,0x32);

	//PULB	
	OpCodes[0x33] = new PULB(Inherent,0x33);

	//PULX	
	OpCodes[0x38] = new PULX(Inherent,0x38);  

	//ROL	
	OpCodes[0x69] = new ROL(Index,0x69);
	OpCodes[0x79] = new ROL(Extended,0x79);

	//ROLA	
	OpCodes[0x49] = new ROLA(Inherent,0x49);

	//ROLB	
	OpCodes[0x59] = new ROLB(Inherent,0x59);

	//ROR	
	OpCodes[0x66] = new ROR(Index,0x66);
	OpCodes[0x76] = new ROR(Extended,0x76);

	//RORA	
	OpCodes[0x46] = new RORA(Inherent,0x46);

	//RORB	
	OpCodes[0x56] = new RORB(Inherent,0x56);

	//RTI	
	OpCodes[0x3b] = new RTI(Inherent,0x3b);

	//RTS	
	OpCodes[0x39] = new RTS(Inherent,0x39);

	//SBA 
	OpCodes[0x10] = new SBA(Inherent,0x10);

	//SBCA	
	OpCodes[0x82] = new SBCA(Immediate,0x82);
	OpCodes[0x92] = new SBCA(Direct,0x92);
	OpCodes[0xa2] = new SBCA(Index,0xa2);
	OpCodes[0xb2] = new SBCA(Extended,0xb2);

	//SBCB	
	OpCodes[0xc2] = new SBCB(Immediate,0xc2);
	OpCodes[0xd2] = new SBCB(Direct,0xd2);
	OpCodes[0xe2] = new SBCB(Index,0xe3);
	OpCodes[0xf2] = new SBCB(Extended,0x72);

	//SCC
	OpCodes[0x0d] = new SCC(Inherent,0x0d);

	//SEI	
	OpCodes[0x0f] = new SEI(Inherent,0x0f);

	//SEV	
	OpCodes[0x0b] = new SEV(Inherent,0x0b);

	//STAA	
	OpCodes[0x97] = new STAA(Direct,0x97);
	OpCodes[0xa7] = new STAA(Index,0xa7);
	OpCodes[0xb7] = new STAA(Extended,0xb7);

	//STAB	
	OpCodes[0xd7] = new STAB(Direct,0xd7);
	OpCodes[0xe7] = new STAB(Index,0xe7);
	OpCodes[0xf7] = new STAB(Extended,0xf7);

	//STD	
	OpCodes[0xdd] = new STD(Direct,0xdd);
	OpCodes[0xed] = new STD(Index,0xed);
	OpCodes[0xfd] = new STD(Extended,0xfd);

	//STS	
	OpCodes[0x9f] = new STS(Direct,0x9f);
	OpCodes[0xaf] = new STS(Index,0xaf);
	OpCodes[0xbf] = new STS(Extended,0xbf);

	//STX	
	OpCodes[0xdf] = new STX(Direct,0xdf);
	OpCodes[0xef] = new STX(Index,0xef);
	OpCodes[0xff] = new STX(Extended,0xff);

	//SUBA	
	OpCodes[0x80] = new SUBA(Immediate,0x80);
	OpCodes[0x90] = new SUBA(Direct,0x90);
	OpCodes[0xa0] = new SUBA(Index,0xa0);
	OpCodes[0xb0] = new SUBA(Extended,0xb0);

	//SUBB	
	OpCodes[0xc0] = new SUBB(Immediate,0xc0);
	OpCodes[0xd0] = new SUBB(Direct,0xd0);
	OpCodes[0xe0] = new SUBB(Index,0xe0);
	OpCodes[0xf0] = new SUBB(Extended,0xf0);

	//SUBD	
	OpCodes[0x83] = new SUBD(Immediate,0x83);
	OpCodes[0x93] = new SUBD(Direct,0x93);
	OpCodes[0xa3] = new SUBD(Index,0xa3);
	OpCodes[0xb3] = new SUBD(Extended,0xb3);

	//SWI	
	OpCodes[0x3f] = new SWI(Inherent,0x3f);  

	//TAB	
	OpCodes[0x16] = new TAB(Inherent,0x16);

	//TAP	
	OpCodes[0x06] = new TAP(Inherent,0x06);

	//TBA	
	OpCodes[0x17] = new TBA(Inherent,0x17);

	//TPA	
	OpCodes[0x07] = new TPA(Inherent,0x07);

	//TST	
	OpCodes[0x6d] = new TST(Index,0x6d);
	OpCodes[0x7d] = new TST(Extended,0x7d);

	//TSTA	
	OpCodes[0x4d] = new TSTA(Inherent,0x4d);

	//TSTB	
	OpCodes[0x5d] = new TSTB(Inherent,0x5d);

	//TSX	
	OpCodes[0x30] = new TSX(Inherent,0x30);

	//TXS 
	OpCodes[0x35] = new TXS(Inherent,0x35);

	//WAI 
	OpCodes[0x3e] = new WAI(Inherent,0x3e);
  
//  OpCodes[0x00] = new Error(0x00);
//  OpCodes[0x02] = new Error(0x02);
	OpCodes[0x00] = new Exit(Immediate,0x00);
	OpCodes[0x02] = new Print(Immediate,0x02);
  
	OpCodes[0x03] = new Error(Immediate,0x03);
	OpCodes[0x12] = new Error(Immediate,0x12);
	OpCodes[0x13] = new Error(Immediate,0x13);
	OpCodes[0x14] = new Error(Immediate,0x14);
	OpCodes[0x15] = new Error(Immediate,0x15);
	OpCodes[0x18] = new Error(Immediate,0x18);
	OpCodes[0x1a] = new Error(Immediate,0x1a);
	OpCodes[0x1c] = new Error(Immediate,0x1c);
	OpCodes[0x1d] = new Error(Immediate,0x1d);
	OpCodes[0x1e] = new Error(Immediate,0x1e);
	OpCodes[0x1f] = new Error(Immediate,0x1f);
	OpCodes[0x41] = new Error(Immediate,0x41);
	OpCodes[0x42] = new Error(Immediate,0x42);
	OpCodes[0x45] = new Error(Immediate,0x45);
	OpCodes[0x4b] = new Error(Immediate,0x4b);
	OpCodes[0x4e] = new Error(Immediate,0x4e);
	OpCodes[0x51] = new Error(Immediate,0x51);
	OpCodes[0x52] = new Error(Immediate,0x52);
	OpCodes[0x55] = new Error(Immediate,0x55);
	OpCodes[0x5b] = new Error(Immediate,0x5b);
	OpCodes[0x5e] = new Error(Immediate,0x5e);
	OpCodes[0x61] = new Error(Immediate,0x61);
	OpCodes[0x62] = new Error(Immediate,0x62);
	OpCodes[0x65] = new Error(Immediate,0x65);
	OpCodes[0x6b] = new Error(Immediate,0x6b);
	OpCodes[0x71] = new Error(Immediate,0x71);
	OpCodes[0x72] = new Error(Immediate,0x72);
	OpCodes[0x75] = new Error(Immediate,0x75);
	OpCodes[0x7b] = new Error(Immediate,0x7b);
	OpCodes[0x87] = new Error(Immediate,0x87);
	OpCodes[0x8f] = new Error(Immediate,0x8f);
	OpCodes[0xc7] = new Error(Immediate,0xc7);
	OpCodes[0xcd] = new Error(Immediate,0xcd);
	OpCodes[0xcf] = new Error(Immediate,0xcf);

	Debug = 0;
  
}//MC10base constructor
*/
#endif
