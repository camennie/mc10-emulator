#ifndef __x6803OPS_H
#define __x6803OPS_H

#include<stdlib.h>
#include"6803.h"
#include"ComputerBase.h"

extern ComputerBase *Computer;


inline unsigned tt8bits x6803base::FlagsToVariable(void)
{
	scratch = Carry_Flag*0x01 + Overflow_Flag*0x02 + Zero_Flag*0x04 + Negative_Flag*0x08 +
         Interrupt_Flag*0x10 + HalfCarry_Flag*0x20;

	return scratch;
}//FlagsToVariable

inline void x6803base::VariableToFlags(unsigned tt8bits CCR)
{
  Carry_Flag = (CCR & 0x01);
  Overflow_Flag = (CCR & 0x02) >> 1;
  Zero_Flag = (CCR & 0x04) >> 2;
  Negative_Flag = (CCR & 0x08) >> 3;
  Interrupt_Flag = (CCR & 0x10) >> 4;
  HalfCarry_Flag = (CCR & 0x20) >> 5;
}//VariableToFlags

inline unsigned tt8bits x6803base::Fetch(void)
{
	scratch = FetchMemory(Program_Counter);
	
	Program_Counter++;

	return scratch;
//	return FetchMemory(Program_Counter-1);
}//Fetch

inline unsigned tt8bits x6803base::FetchMemory(unsigned tt16bits pos)
{
//if((Mode==Index)||(Mode==Extended))
//	LastPosition=pos;

  return Computer->FetchMemory(pos);
}//FetchMemory

inline unsigned tt16bits x6803base::FetchAddress(void)
{
  switch(Mode)
  {
    case Direct:
      return Fetch();
    case Index:
      return Fetch() + Index_Pointer;
    case Extended:
      return (Fetch() << 8) + Fetch();
    default:
      printf("Tried fetching FetchAddress with mode %d\n",Mode);
      return 0;
  }//switch
}//FetchAddress

inline unsigned tt8bits x6803base::FetchData(void)
{
	switch(Mode)
	{
		case Immediate:
	    case Relative:
	      	return Fetch();
	    case Index:
	      	return FetchMemory(Fetch() + Index_Pointer);
	    case Direct:
			return FetchMemory(Fetch());
	    case Extended:
    		return FetchMemory((Fetch()<<8) + Fetch());
	    default:
		    printf("Invalid mode %d in function FetchData8 called from %s!!!",Mode, mnemonic);
		    return 0;
	}//switch
}//FetchData

inline unsigned tt16bits x6803base::FetchData16(void)
{
  switch(Mode)
  {
    case Immediate:
      return (Fetch()<<8) + Fetch();
      break;
    case Index:
      scratch16 = Fetch() + Index_Pointer;
      break;
    case Direct:
      scratch16 = Fetch();
      break;
    case Extended:
      scratch16 = (Fetch() << 8) + Fetch();
      break;
    default:
      printf("Invalid mode %d in function FetchData8!!!\n",Mode);
      return 0;         
  }//switch

  return (FetchMemory(scratch16) << 8) + FetchMemory(scratch16+1);
}//fetchData16

inline void x6803base::SetMemory(unsigned tt16bits pos, unsigned tt8bits val)
{
  Computer->SetMemory(pos,val);
}//SetMemory

inline void x6803base::SetMemory16(unsigned tt16bits pos, unsigned tt16bits val)
{
  SetMemory(pos,val >> 8);
  SetMemory(pos+1,val & 0xff);
}//SetMemory16

inline void x6803base::SetLastRead(unsigned tt8bits val)
{

  unsigned tt16bits Position;

  switch(Mode)
  {
    case Direct:
      Position = FetchMemory(Program_Counter - 1);
      break;
    case Index:
      Position = FetchMemory(Program_Counter - 1) + Index_Pointer;
      break;
    case Extended:
      Position = (FetchMemory(Program_Counter - 2) << 8) + FetchMemory(Program_Counter - 1);
      break;
    default:
      printf("Tried to set last read with mode %d\n",Mode);
      Position = 0;
  }//switch

	SetMemory(Position,val);

//Position = LastPosition;


//  SetMemory(LastPosition, val);
}//SetLastRead

//stack functions (to reduce code errors)
inline void x6803base::PushStack(unsigned tt8bits val)
{
  SetMemory(Stack_Pointer,val);
  Stack_Pointer--;
}//PushStack

inline void x6803base::PushStack16(unsigned tt16bits val)
{
  SetMemory16(Stack_Pointer-1,val);
  Stack_Pointer-=2;  
}//PushStack16

inline unsigned tt8bits x6803base::PopStack(void)
{
  Stack_Pointer++;
  return FetchMemory(Stack_Pointer);
}//PopStack

inline unsigned tt16bits x6803base::PopStack16(void)
{
  Stack_Pointer+=2;
  return FetchMemory(Stack_Pointer) + (FetchMemory(Stack_Pointer-1)<<8);
}//PopStack16

//ALU functions
inline unsigned tt8bits x6803base::Add(unsigned tt8bits first, unsigned tt8bits second)
{
  scratch16 = first + second;
  SET8_HNZVC(first, second,scratch16);

  return (scratch16&0xff);
}//Add

inline unsigned tt8bits x6803base::AddCarry(unsigned tt8bits first, unsigned tt8bits second)
{
	scratch16 = first + second + Carry_Flag;
	SET8_HNZVC(first,second,scratch16);

	return (scratch16&0xff);  
}//AddCarry

inline unsigned tt16bits x6803base::Add16(unsigned tt16bits first, unsigned tt16bits second)
{
  scratch32 = first + second;

  SET16_NZVC(first,second,scratch32);

  return (scratch32&0xffff);
}//Add16

inline unsigned tt8bits x6803base::Subtract(unsigned tt8bits first, unsigned tt8bits second)
{
	scratch16 = first-second;
	SET8_NZVC(first,second,scratch16);

  return (scratch16 & 0xff);
}//Subtract

inline unsigned tt8bits x6803base::SubtractCarry(unsigned tt8bits first, unsigned tt8bits second)
{
	scratch16 = first - second - Carry_Flag;
	SET8_NZVC(first,second,scratch16);

	return (scratch16 & 0xff);
}//SubtractCarry

inline unsigned tt16bits x6803base::Subtract16(unsigned tt16bits first, unsigned tt16bits second)
{
	scratch32 = first - second;
  
	SET16_NZVC(first,second,scratch32);

	return (scratch32 & 0xffff);
}//Subtract16
 
//Now the logical operations:
inline unsigned tt8bits x6803base::AND(unsigned tt8bits first, unsigned tt8bits second)
{
  unsigned tt8bits result = first & second;

  SET8_NZ(result);
  Overflow_Flag=0;

  return result;
}//AND

inline unsigned tt8bits x6803base::Complement(unsigned tt8bits first)
{
  unsigned tt8bits result = first ^ 0xff;

  SET8_NZ(result);
  Overflow_Flag = 0;
  Carry_Flag = 1;

  return result;
}//Complement

inline unsigned tt8bits x6803base::EOR(unsigned tt8bits first, unsigned tt8bits second)
{
  unsigned tt8bits result = first ^ second;

  SET8_NZ(result);
  Overflow_Flag = 0;

  return result;
}//EOR

inline unsigned tt8bits x6803base::Negate(unsigned tt8bits first)
{
  unsigned tt8bits result = (first ^ 0xff) + 1;

  SET8_NZ(result);

  Overflow_Flag = (result == 0x80);
  Carry_Flag = (result == 0x00);

  return result;

//  return Subtract(0,first);
}//Negate

inline unsigned tt8bits x6803base::OR(unsigned tt8bits first, unsigned tt8bits second)
{
  unsigned tt8bits result = first | second;

  SET8_NZ(result);
  Overflow_Flag = 0;

  return result;
}//OR

inline unsigned tt8bits x6803base::Decrement(unsigned tt8bits first)
{
  //should overflow be set?
  Overflow_Flag = (first == 0x80);

  SET8_NZ(first-1);

  return first-1;
}//Decrement

inline unsigned tt8bits x6803base::Increment(unsigned tt8bits first)
{
  //should overflow be set?
  Overflow_Flag = (first == 0x7f);

  SET8_NZ(first+1);

  return first+1;
}//Increment

//Handle shifting
#define ArithmeticShift 1
#define LogicalShift 2
#define Rotate 3

inline unsigned tt8bits x6803base::ShiftLeft(unsigned tt8bits first, int ShiftMode)
{
  unsigned tt8bits result = (first << 1);

  //we only need to change the result if it's a rotate
  if(ShiftMode==Rotate)
    result += Carry_Flag;

  Carry_Flag = ((first & 0x80) >> 7);

  SET8_NZ(result);
      
  //check for overflow
  //if( ((first&0x80)>>7)^((first&0x40)>>6)) //remember >>7 and >>6 to get the bits..
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;

  return result;
}//ShiftLeft

inline unsigned tt8bits x6803base::ArithmeticShiftLeft(unsigned tt8bits first)
{
  return ShiftLeft(first,ArithmeticShift);
}//ArithmeticShiftLeft

inline unsigned tt8bits x6803base::RotateLeft(unsigned tt8bits first)
{
  return ShiftLeft(first,Rotate);
}//RotateLeft

inline unsigned tt16bits x6803base::ShiftLeft16(unsigned tt16bits first)
{
  unsigned tt16bits result = (first << 1);

  //remember that the only left double shift operation for the 6803 is logical/arithmetic shift

  Carry_Flag = ((first & 0x8000) >> 15);

  SET16_NZ(result);

  //check for overflow
  //if( ((first&0x8000)>>15)^((first&0x4000)>>14))
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;

  return result;
}//ShiftLeft16

inline unsigned tt16bits x6803base::ShiftRight16(unsigned tt16bits first)
{
  unsigned tt16bits result = first >> 1;

  //remember that the only double shift operation for the 6803 is logical/arithmetic shift

  Carry_Flag = (first & 0x01);

  SET16_NZ(result);

  //check for overflow
  //if( ((first&0x8000)>>15)^((first&0x4000)>>14))
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;

  return result;
}//ShiftRight16

inline unsigned tt8bits x6803base::ShiftRight(unsigned tt8bits first, int ShiftMode)
{
  unsigned tt8bits result = (first >> 1);

  switch(ShiftMode)
  {
    case ArithmeticShift:
      result |= (first&0x80);
      break;          
    case Rotate:
      result |= (Carry_Flag << 7);
      break;
    case LogicalShift:
      break;
	default:
		printf("Bad shift!\n");
  }//switch

  Carry_Flag = (first & 0x01);

  //check for overflow
  //if( ((first&0x80)>>7)^((first&0x40)>>6)) //remember >>7 and >>6 to get the bits..
  //  SET_OVERFLOW;
  //else
  //  RESET_OVERFLOW;

  SET8_NZ(result);

  //let's follow what the 6800 data sheet says for overflow
  Overflow_Flag = Negative_Flag ^ Carry_Flag;
 
  return result;
}//ShiftRight8
  
inline unsigned tt8bits x6803base::ArithmeticShiftRight(unsigned tt8bits first)
{
  return ShiftRight(first,ArithmeticShift);
}//ArithmeticShiftRight8

inline unsigned tt8bits x6803base::LogicalShiftRight(unsigned tt8bits first)
{
  return ShiftRight(first,LogicalShift);
}//LogicalShiftRight8

inline unsigned tt8bits x6803base::RotateRight(unsigned tt8bits first)
{
  return ShiftRight(first,Rotate);
}//RotateRight8


class Error : public x6803base
//Catch-all
{
  public:
    Error(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ERR"); MPUcycles=0;};
    void operation(void)
    {
		Computer->HandleInvalidOpCode(opcode);
	
      //printf("Error! Tried to execute an invalid instruction %d!\n\n",opcode);
      //Computer->ShowRegisters();
      
      //printf("Exit 3\n");
      //exit(1);
    }//operation
};//Error

//********************************************************************************//

class Print : public x6803base
//Print out A
{
  public:
    Print(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PNT"); MPUcycles=0;};
    void operation(void)
    {    
      if(Accumulator[A] < 16)
        printf("0");
        
      printf("%x",Accumulator[A]);
    }//operation
};//Print

//********************************************************************************//

class Exit : public x6803base
//Quit
{
  public:
    Exit(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"EXT"); MPUcycles=0;};
    void operation(void)
    {
      printf("Exit Called (Exit 4)\n\n");      
      exit(1);
    }//operation
};//Exit

//********************************************************************************//

class Out : public x6803base
//Out
{
  public:
    Out(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"OUT"); MPUcycles=0;};
    void operation(void)
    {
    }
};//Out

//********************************************************************************//

class CHOUT : public x6803base
//Character Out to console
{
  public:
    CHOUT(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CHO"); MPUcycles=0;};
    void operation(void)
    {
      int src = Fetch();
      
      printf("Was told to print out %x\n\n",src);
      printf("Exit 5\n");
      exit(1);
    }//operation
};//CHOUT

//********************************************************************************//
//********************************************************************************//
//********************************************************************************//

class ABA : public x6803base
//Add accumulator A and B together and store the result in A
{
  public:
    ABA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ABA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Add(Accumulator[A],Accumulator[B]);
    }//opearation
};//ABA class

//********************************************************************************//

class ABX : public x6803base
//Add accumulator B to X
{
  public:
   ABX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ABX"); MPUcycles=3;};
    void operation(void)
    {
      Index_Pointer += Accumulator[B];
    }//operation
};//ABX class

//********************************************************************************//

class ADCA : public x6803base
//Add with carry to A
{
  public:
    ADCA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ADCA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = AddCarry(Accumulator[A],FetchData());
    }//operation
};//ADCA

//********************************************************************************//

class ADCB : public x6803base
//Add with carry to B
{
  public:
    ADCB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ADCB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = AddCarry(Accumulator[B],FetchData());
    }//operation
};//ADCB

//********************************************************************************//

class ADDA : public x6803base
//Add to A
{
  public:
    ADDA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ADDA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Add(Accumulator[A],FetchData());
    }//operation
};//ADDA

//********************************************************************************//

class ADDB : public x6803base
//Add to B
{
  public:
    ADDB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ADDB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B]= Add(Accumulator[B],FetchData());
    }//operation
};//ADDB

//********************************************************************************//

class ADDD : public x6803base
//Add D
{
  public:
    ADDD(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ADDD"); MPUcycles=4;};
    void operation(void)
    {
      *Accumulator_D = Add16(*Accumulator_D,FetchData16());
    }//operation
};//ADDD

//********************************************************************************//

class ANDA : public x6803base
//And
{
  public:
    ANDA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ANDA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = AND(Accumulator[A],FetchData());
    }//operation
};//ANDA

//********************************************************************************//

class ANDB : public x6803base
//And
{
  public:
    ANDB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ANDB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = AND(Accumulator[B],FetchData());
    }//operation
};//ANDB

//********************************************************************************//

class ASL : public x6803base
//Arithmetic shift left 
{
  public:
    ASL(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASL"); MPUcycles=6;};
    void operation(void)
    {
      scratch = ArithmeticShiftLeft(FetchData());
      //SetMemory(LastPosition, scratch);
	  SetLastRead(scratch);
    }//operation
};//ASL

//********************************************************************************//

class ASLA : public x6803base
//Arithmetic shift left 
{
  public:
    ASLA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASLA"); MPUcycles=2;};
    void operation(void)
    { 
      Accumulator[A] = ArithmeticShiftLeft(Accumulator[A]);
    }//operation
};//ASLA

//********************************************************************************//

class ASLB : public x6803base
//Arithmetic shift left 
{
  public:
    ASLB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASLB"); MPUcycles=2;};
    void operation(void)
    {            
      Accumulator[B] = ArithmeticShiftLeft(Accumulator[B]);
    }//operation
};//ASLB

//********************************************************************************//

class ASLD : public x6803base
//Arithmetic shift left 
{
  public:
    ASLD(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASLD"); MPUcycles=3;};
    void operation(void)
    { 
      *Accumulator_D = ShiftLeft16(*Accumulator_D);
    }//operation
};//ASLB

//********************************************************************************//

class ASR : public x6803base
//Arithmetic shift right
{
  public:
    ASR(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASR"); MPUcycles=6;};
    void operation(void)
    {
      scratch = ArithmeticShiftRight(FetchData());
      //SetMemory(LastPosition, scratch);
	  SetLastRead(scratch);
    }//operation
};//ASR class 

//********************************************************************************//

class ASRA : public x6803base
//Arithmetic shift right of accumulator A
{
  public:
    ASRA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASRA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = ArithmeticShiftRight(Accumulator[A]);
    }//operation
};//ASRA class 

//********************************************************************************//

class ASRB : public x6803base
//Arithmetic shift right of accumulator B
{
  public:
    ASRB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ASRB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = ArithmeticShiftRight(Accumulator[B]);
   }//operation
};//ASRB class 

//********************************************************************************//

class BRA : public x6803base
//Branch always
{
  public:
    BRA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BRA"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      Program_Counter += pos;
    }//operation
};//BRA class

//********************************************************************************//

class BRN : public x6803base
//Branch Never
{
  public:
    BRN(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BRN"); MPUcycles=3;};
    void operation(void)
    {
      (void)FetchData();
    }//operation
};//BRN class

//********************************************************************************//

class BCC : public x6803base
//Branch if Carry Clear
{
  public:
    BCC(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BCC"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
            
      if(Carry_Flag==0)
        Program_Counter += pos;
    }//operation
};//BCC class

//********************************************************************************//

class BCS : public x6803base
//Branch if Carry Set
{
  public:
    BCS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BCS"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if(Carry_Flag==1)
        Program_Counter += pos;
    }//operation
};//BCS class

//********************************************************************************//

class BEQ : public x6803base
//Branch if Equal to Zero
{
  public:
    BEQ(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BEQ"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if(Zero_Flag==1)
        Program_Counter += pos;
    }//operation
};//BEQ class

//********************************************************************************//

class BGE : public x6803base
//Branch if >= Zero
{
  public:
    BGE(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BGE"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      
      if( (Negative_Flag^Overflow_Flag)==0 )
        Program_Counter += pos;              
    }//operation
};//BGE class

//********************************************************************************//

class BGT : public x6803base
//Branch if > Zero
{
  public:
    BGT(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BGT"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if( (Zero_Flag | (Negative_Flag ^ Overflow_Flag)) == 0)
        Program_Counter += pos;
    }//operation
};//BGT class

//********************************************************************************//

class BHI : public x6803base
//Branch if Higher
{
  public:
    BHI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BHI"); MPUcycles=3;};
    void operation(void)
    {      
      signed tt8bits pos = FetchData();

      if((Carry_Flag|Zero_Flag)==0)
        Program_Counter += pos;
    }//operation
};//BHI class

//********************************************************************************//

class BLE : public x6803base
//Branch if <= zero
{
  public:
    BLE(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BLE"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if( (Zero_Flag | (Negative_Flag ^ Overflow_Flag)) == 1)
        Program_Counter += pos;
    }//operation
};//BLE class

//********************************************************************************//

class BLS : public x6803base
//Branch if lower or same
{
  public:
    BLS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BLS"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if( (Carry_Flag | Zero_Flag) == 1)
        Program_Counter += pos;
    }//operation
};//BLS class

//********************************************************************************//

class BLT : public x6803base
//Branch if < zero
{
  public:
    BLT(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BLT"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if( (Negative_Flag ^ Overflow_Flag) == 1)
        Program_Counter += pos;        
    }//operation
};//BLT class

//********************************************************************************//

class BMI : public x6803base
//Branch if minus
{
  public:
    BMI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BMI"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if(Negative_Flag == 1)
        Program_Counter += pos;
    }//operation
};//BMI class

//********************************************************************************//

class BNE : public x6803base
//branch if not equal to zero
{
  public:
    BNE(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BNE"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
   
      if(!Zero_Flag)
        Program_Counter += pos;
    }//operation
};//BNE class

//********************************************************************************//

class BVC : public x6803base
//branch if overflow clear
{
  public:
    BVC(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BVC"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if(Overflow_Flag==0)
        Program_Counter += pos;
    }//operation
};//BVC class

//********************************************************************************//

class BVS : public x6803base
//branch if overflow set
{
  public:
    BVS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BVS"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if(Overflow_Flag == 1)
        Program_Counter += pos;
    }//operation
};//BVS class

//********************************************************************************//

class BPL : public x6803base
//branch if plus
{
  public:
    BPL(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BPL"); MPUcycles=3;};
    void operation(void)
    {
      signed tt8bits pos = FetchData();
      
      if(Negative_Flag==0)
        Program_Counter += pos;
    }//operation
};//BPL class

//********************************************************************************//

class BSR : public x6803base
//Branch to Subroutine
{
  public:
    BSR(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BSR"); MPUcycles=6;};
    void operation(void)
    {
      signed tt8bits pos = Fetch();

      PushStack16(Program_Counter);
    
      Program_Counter += pos;      
    }//operation    
};//BSR class

//********************************************************************************//

class BITA : public x6803base
//Bit Test A
{
  public:
    BITA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BITA"); MPUcycles=2;};
    void operation(void)
    {
      (void)AND(Accumulator[A],FetchData());
   }//operation
};//BITA

//********************************************************************************//

class BITB : public x6803base
//Bit Test B
{
  public:
    BITB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"BITB"); MPUcycles=2;};
    void operation(void)
    {
      (void)AND(Accumulator[B],FetchData());
   }//operation
};//BITB

//********************************************************************************//

class CBA : public x6803base
//Compare Accumulators
{
  public:
    CBA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CBA"); MPUcycles=2;};
    void operation(void)
    {
      (void)Subtract(Accumulator[A],Accumulator[B]);
    }//operation
};//CBA

//********************************************************************************//

class CLC : public x6803base
//Clear Carry
{
  public:
    CLC(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CLC"); MPUcycles=2;};
    void operation(void)
    {
      Carry_Flag = 0;
    }//operation
};//CLC class

//********************************************************************************//

class CLI : public x6803base
//Clear Interrupt Mask
{
  public:
    CLI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CLI"); MPUcycles=2;};
    void operation(void)
    {
      Interrupt_Flag = 0;
    }//operation

};//CLI class

//********************************************************************************//

class CLR : public x6803base
//Clear
{
  public:
    CLR(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CLR"); MPUcycles=6;};
    void operation(void)
    {
      SetMemory(FetchAddress(), 0);

      Zero_Flag = 1;
      Negative_Flag = 0;
      Overflow_Flag = 0;
      Carry_Flag = 0;
    }//operation
};//CLR

//********************************************************************************//

class CLRA : public x6803base
//Clear Accumulator A
{
  public:
    CLRA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CLRA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = 0;

      Negative_Flag=0;
      Overflow_Flag=0;
      Carry_Flag=0;
      Zero_Flag=1;
    }//operation
};//CLRA class

//********************************************************************************//

class CLRB : public x6803base
//Clear Accumulator B
{
  public:
    CLRB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CLRB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = 0;
      
      Negative_Flag=0;
      Overflow_Flag=0;
      Carry_Flag=0;
      Zero_Flag=1;
    }//operation
};//CLRB class

//********************************************************************************//

class CLV : public x6803base
//Clear Overflow
{
  public:
    CLV(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CLV"); MPUcycles=2;};
    void operation(void)
    {
      Overflow_Flag = 0;
    }//operation
};//CLV class

//********************************************************************************//

class CMPA : public x6803base
//Compare A
{
  public:
    CMPA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CMPA"); MPUcycles=2;};
    void operation(void)
    {
      (void)Subtract(Accumulator[A],FetchData());
    }//operation
};//CMPA class
    
//********************************************************************************//

class CMPB : public x6803base
//Compare B
{
  public:
    CMPB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CMPB"); MPUcycles=2;};
    void operation(void)
    {
      (void)Subtract(Accumulator[B],FetchData());
    }//operation
};//CMPB class
    
//********************************************************************************//

class CPX : public x6803base
//Compare Index Register
{
  public:
    CPX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"CPX"); MPUcycles=4;};
    void operation(void)
    {
      (void)Subtract16(Index_Pointer,FetchData16());
    }//operation
};//CPX_Immediate

//********************************************************************************//

class COM : public x6803base
//1's complement of indexed position
{
  public:
    COM(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"COM"); MPUcycles=6;};
    void operation(void)
    {
      scratch = Complement(FetchData());
	  //SetMemory(LastPosition, scratch);
      SetLastRead(scratch);
    }//operation
};//COM class

//********************************************************************************//

class COMA : public x6803base
//1's compliment of A
{
  public:
    COMA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"COMA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Complement(Accumulator[A]);
    }//operation
};//COMA class

//********************************************************************************//

class COMB : public x6803base
//1's compliment of B
{
  public:
    COMB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"COMB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = Complement(Accumulator[B]);
   }//operation
};//COMB class

//********************************************************************************//

class DAA : public x6803base
//Decimal Adjust A
{
  public:
    DAA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"DAA"); MPUcycles=2;};
    void operation(void)
    {
      unsigned char msd,lsd,addfactor=0,tmpcarry;
      
      msd = (Accumulator[A] >> 4);
      lsd = (Accumulator[A] << 4);
      lsd = (lsd >> 4);
      
      tmpcarry = Carry_Flag;
      
      printf("msd: %d	lsd: %d\n",msd,lsd);
      
      if( (HalfCarry_Flag)||(lsd>9) )
      {
        printf("one\n");
        lsd += 6;
        
        if(lsd&16)
        {
          lsd ^= 16;
          msd += 1;
        }
        
//        addfactor += 6;
      }//if
        
      if( (Carry_Flag)||(msd>9) ) // ||( (msd>8)||(lsd>9) ) )
      {
        printf("two\n");
      
        msd += 6;
        
        if(msd&16)
          tmpcarry=1;
        
//        addfactor += 96;
      }//if
        
//      if(addfactor)
//        Accumulator[A] = Add(Accumulator[A],addfactor,0);
      printf("msd: %d	lsd: %d\n",msd,lsd);
      
      
      msd = (msd << 4);
      
      Accumulator[A] = msd + lsd;
      
      if(Accumulator[A]==0)
        Zero_Flag=1;
      else
        Zero_Flag=0;
        
      if(Accumulator[A]&128)
        Negative_Flag=1;
      else
        Negative_Flag=0;
        
    
      if(HalfCarry_Flag)
        HalfCarry_Flag=0;
        
//      Carry_Flag |= tmpcarry;
    }//operation
};//DAA class

//********************************************************************************//

class DEC : public x6803base
//decrement
{
  public:
    DEC(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"DEC"); MPUcycles=6;};
    void operation(void)
    {
      scratch = Decrement(FetchData());
	  //SetMemory(LastPosition, scratch);
      SetLastRead(scratch);
    }//operation
};//DEC


//********************************************************************************//

class DECA : public x6803base
//Decrement accumulator A
{
  public:
    DECA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"DECA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Decrement(Accumulator[A]);
    }//operation
};//DECA class

//********************************************************************************//

class DECB : public x6803base
//Decrement accumulator B
{
  public:
    DECB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"DECB"); MPUcycles=2;};
    void operation(void)
    { 
      Accumulator[B] = Decrement(Accumulator[B]);
    }//operation
};//DECB class

//********************************************************************************//

class DES : public x6803base
//Decrement Stack pointer
{
  public:
    DES(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"DES"); MPUcycles=3;};
    void operation(void)
    {
      Stack_Pointer--;      
    }//operation
};//DES class

//********************************************************************************//

class DEX : public x6803base
//Decrement Index register
{
  public:
    DEX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"DEX"); MPUcycles=3;};
    void operation(void)
    {
      Index_Pointer--;

      SET16_Z(Index_Pointer);
    }//operation
};//DEX class

//********************************************************************************//

class EORA : public x6803base
//Inclusive OR of accumulator A
{
  public:
    EORA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"EORA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = EOR(Accumulator[A],FetchData());
   }//operation
};//EORA

//********************************************************************************//

class EORB : public x6803base
//Inclusive OR of accumulator B
{
  public:
    EORB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"EORB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = EOR(Accumulator[B],FetchData());
   }//operation
};//EORB

//********************************************************************************//

class INC : public x6803base
//increment
{
  public:
    INC(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"INC"); MPUcycles=6;};
    void operation(void)
    {
      scratch = Increment(FetchData());
	  //SetMemory(LastPosition, scratch);  
      SetLastRead(scratch);
    }//operation
};//INC class

//********************************************************************************//

class INCA : public x6803base
//Increment accumulator A
{
  public:
    INCA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"INCA"); MPUcycles=2;};
    void operation(void)
    { 
      Accumulator[A] = Increment(Accumulator[A]);
    }//operation
};//INCA class

//********************************************************************************//

class INCB : public x6803base
//Increment accumulator B
{
  public:
    INCB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"INCB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = Increment(Accumulator[B]);
   }//operation
};//INCB class

//********************************************************************************//

class INS : public x6803base
//Increment Stack Pointer
{
  public:
    INS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"INS"); MPUcycles=3;};
    void operation(void)
    {
      Stack_Pointer++;
    }//operation
};//INS class

//********************************************************************************//

class INX : public x6803base
//Increment Index register
{
  public:
    INX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"INX"); MPUcycles=3;};
    void operation(void)
    {
      Index_Pointer++;

      SET16_Z(Index_Pointer);
    }//operation
};//INX class      

//********************************************************************************//

class JMP : public x6803base
//Jump
{
  public:
    JMP(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"JMP"); MPUcycles=3;};
    void operation(void)
    {
      unsigned tt16bits pos;

      if(Mode == Index)
        pos = Index_Pointer + Fetch();
      else
        //Mode == Extended
        pos = FetchAddress();

      Program_Counter = pos;
    }//operation
};//JMP

//********************************************************************************//

class JSR : public x6803base
//Jump to Subroutine
{
  public:
    JSR(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"JSR"); MPUcycles=5;};
    void operation(void)
    {
      unsigned tt16bits val = FetchAddress();

      PushStack16(Program_Counter);

      Program_Counter = val;
    }//operation
};//JSR

//********************************************************************************//

class LDAA : public x6803base
//Load accumulator A
{
  public:
    LDAA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LDAA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = FetchData();

      SET8_NZ(Accumulator[A]);
      Overflow_Flag=0;
    }//operation
};//LDAA

//********************************************************************************//

class LDAB : public x6803base
//Load accumulator B
{
  public:
    LDAB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LDAB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = FetchData();

      SET8_NZ(Accumulator[B]);
      Overflow_Flag=0;
   }//operation
};//LDAB

//********************************************************************************//

class LDD : public x6803base
//Load double
{
  public:
    LDD(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LDD"); MPUcycles=3;};
    void operation(void)
    {
      *Accumulator_D = FetchData16();

      SET16_NZ(*Accumulator_D);
      Overflow_Flag=0;
   }//operation
};//LDD

//********************************************************************************//

class LDS : public x6803base
//Load Stack Pointer
{
  public:
    LDS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LDS"); MPUcycles=3;};
    void operation(void)
    {      
      Stack_Pointer = FetchData16();

      SET16_NZ(Stack_Pointer);
      Overflow_Flag=0;
   }//operation
};//LDS class

//********************************************************************************//

class LDX : public x6803base
//Load index register
{
  public:
    LDX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LDX"); MPUcycles=3;};
    
    void operation(void)
    { 
      scratch16 = FetchData16();

      Index_Pointer = scratch16;

      SET16_NZ(Index_Pointer);
      Overflow_Flag=0;
   }//operation
};//LDX class

//********************************************************************************//

//Remember, the Logical Shift Left operations are the same as the Arithmetic Shift Left

//********************************************************************************//

class LSR : public x6803base
//Logical shift right 
{
  public:
    LSR(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LSR"); MPUcycles=6;};
    void operation(void)
    {
      scratch = LogicalShiftRight(FetchData());
	  //SetMemory(LastPosition, scratch);  
      SetLastRead(scratch);
    }//operation
};//LSR class 

//********************************************************************************//

class LSRA : public x6803base
//Logical shift right of accumulator A
{
  public:
    LSRA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LSRA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = LogicalShiftRight(Accumulator[A]);
   }//operation
};//LSRA class 

//********************************************************************************//

class LSRB : public x6803base
//Logical shift right of accumulator B
{
  public:
    LSRB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LSRB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = LogicalShiftRight(Accumulator[B]);
   }//operation
};//LSRB class 

//********************************************************************************//

class LSRD : public x6803base
//Logical shift right of D
{
  public:
    LSRD(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"LSRD"); MPUcycles=3;};
    void operation(void)
    {
      *Accumulator_D = ShiftRight16(*Accumulator_D);
   }//operation
};//LSRD class

//********************************************************************************//

class MUL : public x6803base
//Multiply [A and B and place it into D]
{
  public:
    MUL(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"MULm"); MPUcycles=10;};
    void operation(void)
    {
      *Accumulator_D = Accumulator[A] * Accumulator[B];

      //this is the only time we're explicitly define a flag.. every other
      //time the carry flag is conditionally set is from addition btw...
      if( (Accumulator[A] * Accumulator[B]) > 0xffff)
        Carry_Flag = 1;
      else
        Carry_Flag = 0;
    }//operation
};//MUL class

//********************************************************************************//

class NEG : public x6803base
//Negate
{
  public:
    NEG(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"NEG"); MPUcycles=6;};
    void operation(void)
    {
      scratch = Negate(FetchData());
	  //SetMemory(LastPosition, scratch);  
      SetLastRead(scratch);
    }//operation
};//NEG

//********************************************************************************//

class NEGA : public x6803base
//Negate A
{
  public:
    NEGA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"NEGA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Negate(Accumulator[A]);
    }//operation
};//NEGA

//********************************************************************************//

class NEGB : public x6803base
//Negate B
{
  public:
    NEGB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"NEGB"); MPUcycles=2;};
    void operation(void)
    {
     Accumulator[B] = Negate(Accumulator[B]);
    }//operation
};//NEGB

//********************************************************************************//

class NOP : public x6803base
//No Operation
{
  public:
    NOP(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"NOP"); MPUcycles=2;};
    void operation(void)
    {
      //well, obviously there's nothing to be done..
    }//operation
};//NOP class

//********************************************************************************//

class ORAA : public x6803base
//Inclusive OR of accumulator A
{
  public:
    ORAA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ORAA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = OR(Accumulator[A],FetchData());
   }//operation
};//ORAA_Immediate

//********************************************************************************//

class ORAB : public x6803base
//Inclusive OR of accumulator B
{
  public:
    ORAB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ORAB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = OR(Accumulator[B],FetchData());
   }//operation
};//ORAB

//********************************************************************************//

class PSHA : public x6803base
//Push A onto stack
{
  public:
    PSHA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PSHA"); MPUcycles=3;};
    void operation(void)
    {
      PushStack(Accumulator[A]);
    }//operation
};//PSHA class

//********************************************************************************//

class PSHB : public x6803base
//Push B onto stack
{
  public:
    PSHB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PSHB"); MPUcycles=3;};
    void operation(void)
    {
      PushStack(Accumulator[B]);
    }//operation
};//PSHB class

//********************************************************************************//

class PSHX : public x6803base
//Push FetchData (X to stack)
{
  public:
    PSHX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PSHX"); MPUcycles=4;};
    void operation(void)
    {
      PushStack16(Index_Pointer);
    }//operation
};//PSHX class

//********************************************************************************//

class PULA : public x6803base
//Pull data from stack to A
{
  public:
    PULA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PULA"); MPUcycles=4;};
    void operation(void)
    {      
      Accumulator[A] = PopStack();
    }//operation
};//PULA class

//********************************************************************************//

class PULB : public x6803base
//Pull data from stack to B
{
  public:
    PULB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PULB"); MPUcycles=4;};
    void operation(void)
    {
      Accumulator[B] = PopStack();
    }//operation
};//PULB class

//********************************************************************************//
      
class PULX : public x6803base
//Pull data from stack to X
{
  public:
    PULX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"PULX"); MPUcycles=5;};
    void operation(void)
    {
      Index_Pointer = PopStack16();
    }//operation
};//PULX class      

//********************************************************************************//

class ROL : public x6803base
//Rotate left 
{
  public:
    ROL(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ROL"); MPUcycles=6;};
    void operation(void)
    {
      scratch = RotateLeft(FetchData());
	  //SetMemory(LastPosition, scratch);  
      SetLastRead(scratch);
    }//operation
};//ROL

//********************************************************************************//

class ROLA : public x6803base
//Rotate accumulator A left
{
  public:
    ROLA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ROLA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = RotateLeft(Accumulator[A]);
   }//operation
};//ROLA

//********************************************************************************//

class ROLB : public x6803base
//rotate accumulator b left
{
  public:
    ROLB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ROLB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = RotateLeft(Accumulator[B]);
   }//operation
};//ROLB

//********************************************************************************//

class ROR : public x6803base
//Rotate right
{
  public:
    ROR(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"ROR"); MPUcycles=6;};
    void operation(void)
    {
      scratch = RotateRight(FetchData());
	  //SetMemory(LastPosition, scratch);  
      SetLastRead(scratch);
    }//operation
};//ROR class 

//********************************************************************************//

class RORA : public x6803base
//rotate right of accumulator A
{
  public:
    RORA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"RORA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = RotateRight(Accumulator[A]);
   }//operation
};//RORA class 

//********************************************************************************//

class RORB : public x6803base
//rotate right of accumulator B
{
  public:
    RORB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"RORB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = RotateRight(Accumulator[B]);
   }//operation
};//RORB class 

//********************************************************************************//

class RTI : public x6803base
//Return from Interrupt
{
  public:
    RTI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"RTI"); MPUcycles=10;};
    void operation(void)
    {
      VariableToFlags(PopStack());
      
      Accumulator[B] = PopStack();
      Accumulator[A] = PopStack();
      Index_Pointer = PopStack16();
      Program_Counter = PopStack16();
    }//operation
};//RTI class     
      
//********************************************************************************//

class RTS : public x6803base
//Return From Subroutine
{
  public:
    RTS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"RTS"); MPUcycles=5;};
    void operation(void)
    {
      Program_Counter = PopStack16();
    }//operation
};//RTS class

//********************************************************************************//

class SBA : public x6803base
//Subtract Accumulator A
{
  public:
    SBA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SBA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Subtract(Accumulator[A],Accumulator[B]);
    }//operation
};//SBA

//********************************************************************************//

class SBCA : public x6803base
//Subtract
{
  public:
    SBCA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SBCA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = SubtractCarry(Accumulator[A],FetchData());
    }//operation
};//SBCA

//********************************************************************************//

class SBCB : public x6803base
//Subtract
{
  public:
    SBCB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SBCB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = SubtractCarry(Accumulator[B],FetchData());
    }//operation
};//SBCB

//********************************************************************************//

class SCC : public x6803base
//NOTE: We call this SCC instead of SEC because on some systems (ie: Suns) SEC is
//		defined as 1, which causes a conflict
//Set Carry
{
  public:
    SCC(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SEC"); MPUcycles=2;};
    void operation(void)
    {
      Carry_Flag = 1;
    }//operation
};//SCC class

//********************************************************************************//

class SEI : public x6803base
//Set Interrupt Mask
{
  public:
    SEI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SEI"); MPUcycles=2;};
    void operation(void)
    {
      Interrupt_Flag = 1;
    }//operation;
};//SEI class

//********************************************************************************//

class SEV : public x6803base
//Set Overflow
{
  public:
    SEV(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SEV"); MPUcycles=2;};
    void operation(void)
    {
      Overflow_Flag = 1;
    }//operation
};//SEV class

//********************************************************************************//

class STAA : public x6803base
//Store accumulator A
{
  public:
    STAA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"STAA"); MPUcycles=3;};
    void operation(void)
    {
      SetMemory(FetchAddress(), Accumulator[A]);

      Overflow_Flag = 0;
      SET8_NZ(Accumulator[A]);
    }//operation
};//STAA

//********************************************************************************//

class STAB : public x6803base
//Store accumulator B
{
  public:
    STAB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"STAB"); MPUcycles=3;};
    void operation(void)
    {
      SetMemory(FetchAddress(), Accumulator[B]);

      Overflow_Flag = 0;
      SET8_NZ(Accumulator[B]);
    }//operation
};//STAB

//********************************************************************************//

class STD : public x6803base
//Store accumulator D
{
  public:
    STD(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"STD"); MPUcycles=5;};
    void operation(void)
    {
      scratch16 = FetchAddress();

      SetMemory16(scratch16,*Accumulator_D);
      
      Overflow_Flag = 0;
      SET16_NZ(*Accumulator_D);
    }//operation
};//STD

//********************************************************************************//

class STS : public x6803base
//Store Stack pointer
{
  public:
    STS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"STS"); MPUcycles=4;};
    void operation(void)
    {
      scratch16 = FetchAddress();

      SetMemory16(scratch16,Stack_Pointer);

      Overflow_Flag = 0;
      SET16_NZ(Stack_Pointer);
    }//operation
};//STS class

//********************************************************************************//

class STX : public x6803base
//Store Index register
{
  public:
    STX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"STX"); MPUcycles=4;};
    void operation(void)
    {
      scratch16 = FetchAddress();
    
      SetMemory16(scratch16,Index_Pointer);

      Overflow_Flag = 0;
      SET16_NZ(Index_Pointer);
    }//operation
};//STX class

//********************************************************************************//

class SUBA : public x6803base
//Subtract
{
  public:
    SUBA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SUBA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Subtract(Accumulator[A],FetchData());
    }//operation
};//SUBA

//********************************************************************************//

class SUBB : public x6803base
//Subtract
{
  public:
    SUBB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SUBB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = Subtract(Accumulator[B],FetchData());
    }//operation
};//SUBB

//********************************************************************************//

class SUBD : public x6803base
//Subtract Double
{
  public:
    SUBD(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SUBD"); MPUcycles=4;};
    void operation(void)
    {
      *Accumulator_D = Subtract16(*Accumulator_D,FetchData16());
    }//operation
};//SUBD

//********************************************************************************//

class SWI : public x6803base
//Software Interrupt
{
  public:
    SWI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SWIm"); MPUcycles=12;};
    void operation(void)
    {    
      //CHANGE!!!
      printf("SWI called!\n");
      printf("Exit 6\n");
      exit(1);
         
      Interrupt_Flag = 1;
      
      //dump all the stuff on the stack...
      PushStack16(Program_Counter);
      PushStack16(Index_Pointer);
      PushStack(Accumulator[A]);
      PushStack(Accumulator[B]);
      PushStack(FlagsToVariable());
      
      Program_Counter = (FetchMemory(0xfffa) << 8) + FetchMemory(0xfffb);
            
    }//operation
};//SWI class

//********************************************************************************//

class TAB : public x6803base
//Transer accumulators [copy A to B]
{
  public:
    TAB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TAB"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[B] = Accumulator[A];
      
      SET8_NZ(Accumulator[B]);
      Overflow_Flag = 0;      
    }//operation
};//TAB class

//********************************************************************************//

class TAP : public x6803base
//Transfer [copy A to CCR]
{
  public:
    TAP(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TAP"); MPUcycles=2;};
    void operation(void)
    {
      VariableToFlags(Accumulator[A]);
    }//operation
};//TAP class

//********************************************************************************//

class TBA : public x6803base
//Transfer accumulators [copy B to A]
{
  public:
    TBA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TBA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = Accumulator[B];

      SET8_NZ(Accumulator[A]);
      Overflow_Flag=0;
    }//operation
};//TBA class

//********************************************************************************//

class TPA : public x6803base
//Transer [copy CCR to A]
{
  public:
    TPA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TPA"); MPUcycles=2;};
    void operation(void)
    {
      Accumulator[A] = FlagsToVariable();
    }//operation
};//TPA class

//********************************************************************************//

class TST : public x6803base
//Test if Zero or Minus
{
  public:
    TST(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TST"); MPUcycles=6;};
    void operation(void)
    {
//CHANGE!!!	
      //(void)Subtract(FetchData(),0);
	  Overflow_Flag=0;
	  Carry_Flag=0;
	  SET8_NZ(FetchData());
    }//operation
};//TST class

//********************************************************************************//

class TSTA : public x6803base
//Test if Zero or Minus
{
  public:
    TSTA(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TSTA"); MPUcycles=2;};
    void operation(void)
    {
//CHANGE!!!	
      //(void)Subtract(Accumulator[A],0);
	  Overflow_Flag=0;
	  Carry_Flag=0;
	  SET8_NZ(Accumulator[A]);
    }//operation
};//TSTA class

//********************************************************************************//

class TSTB : public x6803base
//Test if Zero or Minus
{
  public:
    TSTB(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TSTB"); MPUcycles=2;};
    void operation(void)
    {
//CHANGE!!!	
      //(void)Subtract(Accumulator[B],0);
	  Overflow_Flag=0;
	  Carry_Flag=0;
	  SET8_NZ(Accumulator[B]);
   }//operation
};//TSTB class

//********************************************************************************//

class TSX : public x6803base
//Transfer [copy S to X]
{
  public:
    TSX(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TSX"); MPUcycles=3;};
    void operation(void)
    {
      Index_Pointer = Stack_Pointer + 1; //is it supposed to be S or S+1? 
    }//operation
};//TSX class

//********************************************************************************//

class TXS : public x6803base
//Transfer [copy X to S]
{
  public:
    TXS(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"TXS"); MPUcycles=3;};
    void operation(void)
    {
      Stack_Pointer = Index_Pointer - 1; //is it supposed to be just X or X-1?
    }//operation
};//TXS class

//********************************************************************************//

class WAI : public x6803base
//Software Interrupt
{
  public:
    WAI(int a, int b) : x6803base(a,b) { strcpy(mnemonic,"SWI"); MPUcycles=9;};
    void operation(void)
    {
      //dump all the stuff on the stack...
      PushStack16(Program_Counter);
      PushStack16(Index_Pointer);
      PushStack(Accumulator[A]);
      PushStack(Accumulator[B]);
      PushStack(FlagsToVariable());
            
      printf("WAI called!\n");
    }//operation
};//WAI class

//********************************************************************************//

#endif

