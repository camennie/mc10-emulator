#ifndef __x6847X11_H
#define __x6847X11_H

#include <X11/Xlib.h>
#include "6847.h"

//Define graphics for X11 systems

class SysDep6847 : x6847base
{
	public:
		void Init(void); //Create the 256x192 window
		void Quit(void); //Close up everything
		void UpdateDisplay(unsigned tt16bits pos, unsigned tt8bits value);
		void UpdateChip(unsigned tt8bits val);
		SysDep6847();
		int PollKey();
		void InitKeys();
		unsigned char Port1[8];
		unsigned char Port2[8];
		int ResetButton;
	
	private:
		Display *display;
		int screen;
		Window window;
		Colormap colourmap;
		GC gc;
		XGCValues xgcvalues;
		Visual *visual;

		int VideoMode;
		int bgColour;
		int fgColour;

		//Colours
		XColor *xBlack;
		XColor *xGreen;
		XColor *xYellow;
		XColor *xBlue;
		XColor *xRed;
		XColor *xBuff;
		XColor *xCyan;
		XColor *xMagenta;
		XColor *xOrange;


};//SysDep6847

#endif
