#ifndef __x6803_H
#define __x6803_H

#include "ComputerBase.h"

//extern ComputerBase *Computer;

//define the order of the registers
//this'll need to be reversed depending on the architecture
#define A 1
#define B 0

//define the addressing modes for fetching
#define Direct      1
#define Index       2
#define Extended    3
#define Relative    4
#define Immediate   5
#define Inherent    6

//To make things faster..
//unsigned tt8bits scratch;
//unsigned tt16bits scratch16;
//unsigned tt32bits scratch32;

//********************************************************************************//

class x6803base
{
	public:
		char mnemonic[6];
		unsigned tt8bits opcode;

		tt8bits MPUcycles;
		tt8bits ProgramBytes;
		tt8bits Mode;

		unsigned tt8bits scratch;
		unsigned tt16bits scratch16;
		unsigned tt32bits scratch32;

//static unsigned tt16bits LastPosition;

    //the status flags
    static int Carry_Flag;
    static int Overflow_Flag;
    static int Zero_Flag;
    static int Negative_Flag;
    static int Interrupt_Flag;
    static int HalfCarry_Flag;

    //the registers
    static unsigned tt8bits Accumulator[2];
    static unsigned tt16bits *Accumulator_D;
    static unsigned tt16bits Index_Pointer;
    static unsigned tt16bits Stack_Pointer;
    static unsigned tt16bits Program_Counter;
    inline unsigned tt8bits FlagsToVariable(void);
    inline void VariableToFlags(unsigned tt8bits);

    inline void SET8_N(unsigned tt8bits var) { Negative_Flag = (var & 0x80) >> 7; };
    inline void SET8_Z(unsigned tt8bits var) { Zero_Flag = !var; };
    inline void SET16_N(unsigned tt16bits var) { Negative_Flag = (var & 0x8000) >> 15; };
    inline void SET16_Z(unsigned tt16bits var) { Zero_Flag = !var; };
    inline void SET8_H(unsigned tt8bits first, unsigned tt8bits second, unsigned tt16bits result) 
    { 
      HalfCarry_Flag = ((first^second^result)&0x10) >> 3; 
    };
    inline void SET8_V(unsigned tt8bits first, unsigned tt8bits second, unsigned tt16bits result) 
    { 
      Overflow_Flag = ((first^second^result^(result>>1))&0x80)>>7; 
    };
    inline void SET16_V(unsigned tt16bits first, unsigned tt16bits second, unsigned tt32bits result) 
    { 
      Overflow_Flag = ((first^second^result^(result>>1))&0x8000)>>15; 
    };
    inline void SET8_NZ(unsigned tt8bits var) {SET8_N(var); SET8_Z(var);};
    inline void SET16_NZ(unsigned tt16bits var) {SET16_N(var); SET16_Z(var);};
    inline void SET8_C(unsigned tt16bits result) 
    { 
      Carry_Flag = (result & 0x100) >> 8;
    };
    inline void SET16_C(unsigned tt32bits result) 
    { 
      Carry_Flag = (result & 0x10000) >> 16;
    };

	inline void SET8_HNZVC(unsigned tt8bits first, unsigned tt8bits second, unsigned tt16bits result)
	{
		SET8_NZVC(first,second,result);
		SET8_H(first, second, result);
	};//SET8_HNZVC
	inline void SET8_NZVC(unsigned tt8bits first, unsigned tt8bits second, unsigned tt16bits result) 
	{
    	//SET8_H(first, second, result);
    	SET8_NZ(result&0xff);
    	SET8_V(first, second, result);
    	SET8_C(result);
	};//SET8_HNZVC
	
    inline void SET16_NZVC(unsigned tt16bits first, unsigned tt16bits second, unsigned tt32bits result )
    {
      SET16_NZ(result&0xffff);
      SET16_V(first, second, result);
      SET16_C(result);
    }//SET16_NZVC

    //generic functions
    inline unsigned tt8bits Fetch(void);
    inline unsigned tt8bits FetchMemory(unsigned tt16bits);
//    inline unsigned tt8bits Data(void);
//    inline unsigned tt16bits Data16(void);

    //define a constructor and fetches
    x6803base(int mode, int op);
    virtual void operation(void) { };
    inline unsigned tt8bits FetchData(void);
    inline unsigned tt16bits FetchData16(void);
    inline unsigned tt16bits FetchAddress(void);
    inline void SetLastRead(unsigned tt8bits val);
    inline void SetLastRead16(unsigned tt16bits val);
    inline void SetMemory(unsigned tt16bits pos, unsigned tt8bits val);
    inline void SetMemory16(unsigned tt16bits pos, unsigned tt16bits val);

    //stack functions (to reduce code errors)
    inline void PushStack(unsigned tt8bits);
    inline void PushStack16(unsigned tt16bits);
    inline unsigned tt8bits PopStack(void);
    inline unsigned tt16bits PopStack16(void);

    //ALU parts (Should these be defines?)
    inline unsigned tt8bits Add(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt8bits AddCarry(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt16bits Add16(unsigned tt16bits first, unsigned tt16bits second);
    inline unsigned tt8bits Subtract(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt8bits SubtractCarry(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt16bits Subtract16(unsigned tt16bits first, unsigned tt16bits second);
    
    //Now the logical operations:
    inline unsigned tt8bits AND(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt8bits Complement(unsigned tt8bits first);
    inline unsigned tt8bits EOR(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt8bits Negate(unsigned tt8bits first);
    inline unsigned tt8bits OR(unsigned tt8bits first, unsigned tt8bits second);
    inline unsigned tt8bits Decrement(unsigned tt8bits first);
    inline unsigned tt8bits Increment(unsigned tt8bits first);

    //Handle shifting
    inline unsigned tt8bits ShiftLeft(unsigned tt8bits first, int ShiftMode);
    inline unsigned tt8bits ArithmeticShiftLeft(unsigned tt8bits first);
    inline unsigned tt8bits RotateLeft(unsigned tt8bits first);
    inline unsigned tt16bits ShiftLeft16(unsigned tt16bits first);
    inline unsigned tt16bits ShiftRight16(unsigned tt16bits first);
    inline unsigned tt8bits ShiftRight(unsigned tt8bits first, int ShiftMode);
    inline unsigned tt8bits ArithmeticShiftRight(unsigned tt8bits first);
    inline unsigned tt8bits LogicalShiftRight(unsigned tt8bits first);
    inline unsigned tt8bits RotateRight(unsigned tt8bits first);
};//x6803base class

//********************************************************************************//





#endif
