#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <unistd.h>
#include <iostream.h>
#include <ctype.h>

#include "ComputerBase.h"
#include "6847.h"
#include "6847X11.h"
#include "SG4.h"

#include <stdio.h>
#include <stdlib.h>

#define FIX(byte,mask,flag) if (flag) (byte) &= ~(mask); else (byte) |= (mask)

SysDep6847::SysDep6847()
{
	xBlack = (XColor *)malloc(sizeof(XColor));
	xGreen = (XColor *)malloc(sizeof(XColor));
	xYellow = (XColor *)malloc(sizeof(XColor));
	xBlue = (XColor *)malloc(sizeof(XColor));
	xRed = (XColor *)malloc(sizeof(XColor));
	xBuff = (XColor *)malloc(sizeof(XColor));
	xCyan = (XColor *)malloc(sizeof(XColor));
	xMagenta = (XColor *)malloc(sizeof(XColor));
	xOrange = (XColor *)malloc(sizeof(XColor));
}//constructor



int SysDep6847::PollKey(void)
{XEvent event;
 KeySym ks;
 int bitmask=-1;
 char index=-1;
 if (XCheckWindowEvent(display,window,KeyPressMask|KeyReleaseMask,&event))
 {ks=toupper(0xff&XLookupKeysym((XKeyEvent *)&event, 0));
//printf("event.keycode is %i\n",((XKeyEvent *) &event)->keycode);
//printf("%x %c\n",ks,ks);
  if (ks>='A' && ks<='Z')
   FIX(Port1[ks%8],  1<<(ks-'@')/8,event.type==KeyPress); // A - Z
  else if (ks==0x0d)
   FIX(Port1[6],     1<<3,event.type==KeyPress); // ENTER
  else if (ks==' ')
   FIX(Port1[7],     1<<3,event.type==KeyPress); // SPACEBAR
  else if (ks>='0' && ks<='7')
   FIX(Port1[ks-'0'],1<<4,event.type==KeyPress); // 0-7
  else if (ks=='-')         
   FIX(Port1[2],     1<<5,event.type==KeyPress); // emulate ':' with '-'
  else if (ks=='=')
   FIX(Port1[5],     1<<5,event.type==KeyPress); // emulate '-' with '='
  else if (ks=='[')
   FIX(Port1[0],     1<<0,event.type==KeyPress); // emulate '@' with '['
  else if (ks>='8' && ks<=';')
   FIX(Port1[ks-'8'],1<<5,event.type==KeyPress); // 8 9 ;
  else if (ks>=',' && ks<='/')
   FIX(Port1[ks-'('],1<<5,event.type==KeyPress); // , [-] . / 
  else if (ks == /* 0xe1*/ 0xe3)
   FIX(Port2[0],0x02,event.type==KeyPress);      // CONTROL
  else if (ks == 0xe2 || ks == 0xe1)         
   FIX(Port2[7],0x02,event.type==KeyPress);      // SHIFT
  else if (ks == 0xff)
   FIX(Port2[2],0x02,event.type==KeyPress);      // delete = BREAK 
  else if (ks == 0x1b)
   FIX(ResetButton,0xFF,event.type==KeyPress);   // escape = RESET
  else
   printf("Unrecognized keysym 0x%0x (%c)\n",ks,ks);
//printf("ResetButton=%i\n",ResetButton);
//printf("%02x%02x%02x%02x%02x%02x%02x%02x - ",
//       Port1[0],Port1[1],Port1[2],Port1[3],
//       Port1[4],Port1[5],Port1[6],Port1[7]);
//printf("%02x%02x%02x%02x%02x%02x%02x%02x\n",
//       Port2[0],Port2[1],Port2[2],Port2[3],
//       Port2[4],Port2[5],Port2[6],Port2[7]);

	return ks;

}}//if //PollKey

void SysDep6847::InitKeys(void)
{	int i;
	ResetButton=0xFF;
	for (i=0;i<8;i++)
		Port1[i]=Port2[i]=0xff;
}

void SysDep6847::Init(void)
{	XSetWindowAttributes attr;
	unsigned long mask = 0L;

	InitKeys();

	display = XOpenDisplay( (char *)NULL);

	if(display==NULL)
	{
		cout << "Couldn't open display! Exiting!\n\n" << endl;
		exit(1);
	}//if

	screen = DefaultScreen(display);
	attr.event_mask = KeyPressMask | KeyReleaseMask;
	window = XCreateWindow(display,
	                       RootWindow(display,screen),
	                       100,100,512,384,5,0,
	                       InputOutput, (Visual *) CopyFromParent,
	                       CWBackPixel|CWBorderPixel|CWEventMask, &attr);

	gc = XCreateGC(display,window,0L,&xgcvalues);

	XMapRaised(display,window);
	XFlush(display);

	visual = DefaultVisual(display,screen);
//	colourmap = XCreateColormap(display,window,visual,AllocNone);
//	XSetWindowColormap(display,window,colourmap);
	colourmap = DefaultColormap(display,screen);

	//Set up each colour
	xBlack->red=0;
	xBlack->green=0;
	xBlack->blue=0;
	xBlack->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xBlack);

	xGreen->red=0;
	xGreen->green=0xffff;
	xGreen->blue=0;
	xGreen->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xGreen);
	
	//might be off (on green)
	xYellow->red=0xffff;
	xYellow->green=0xe700;
	xYellow->blue=0;
	xYellow->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xYellow);
	
	xBlue->red=0;
	xBlue->green=0;
	xBlue->blue=0xffff;
	xBlue->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xBlue);
	
	xRed->red=0xffff;
	xRed->green=0;
	xRed->blue=0;
	xRed->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xRed);
	
	xBuff->red=0xffff;
	xBuff->green=0xffff;
	xBuff->blue=0xffff;
	xBuff->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xBuff);
	
	xCyan->red=0;
	xCyan->green=0xffff;
	xCyan->blue=0xffff;
	xCyan->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xCyan);
	
	xMagenta->red=0xffff;
	xMagenta->green=0;
	xMagenta->blue=0xffff;
	xMagenta->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xMagenta);
	
	//might be off (on green)
	xOrange->red=0xffff;
	xOrange->green=0xae13;
	xOrange->blue=0;
	xOrange->flags=DoRed|DoGreen|DoBlue;
	XAllocColor(display,colourmap,xOrange);
}//Init

void SysDep6847::UpdateDisplay(unsigned tt16bits pos, unsigned tt8bits val)
{
	int screenX, screenY;
	unsigned char Block[12];
	unsigned char flag=0x00;
	unsigned long colourindex;
	unsigned long bgcolourindex = xBlack->pixel;
	unsigned long colour;
	int i,j,cpos;

//	printf("Tried to write to video ram with %x (%c) at %x.\n",val,val,pos);

	if(val<=0x7f)
	{
		colourindex = xGreen->pixel;
		
		if(val>0x3f)
		{
			flag=0xff;
			val-=0x40;
		}//if

		for(i=0;i<12;i++)
			Block[i] = SG4CharacterSet[val][i];
			
//		Block = &SG4CharacterSet[val][0];

//printf("character is %x\n",val);		
	}
	else
	{
		for(i=0;i<12;i++)
			Block[i] = SG4Rectangle[val&0x0f][i];
//		Block = SG4Rectangle[val&0x0f];

		switch(val&0xf0)
		{
			case 0x80:
				colourindex = xGreen->pixel;
				break;
			case 0x90:
				colourindex = xYellow->pixel;
				break;
			case 0xa0:
				colourindex = xBlue->pixel;
				break;
			case 0xb0:
				colourindex = xRed->pixel;
				break;
			case 0xc0:
				colourindex = xBuff->pixel;
				break;
			case 0xd0:
				colourindex = xCyan->pixel;
				break;
			case 0xe0:
				colourindex = xMagenta->pixel;
				break;
			case 0xf0:
				colourindex = xOrange->pixel;
				break;
		}//switch
	}//if

	screenX = (pos%32);
	screenY = ((pos-screenX)/32);

	screenX *= 16;
	screenY *= 24;

//printf("(x,y): (%d,%d)\n",screenX,screenY);

//if(val!=0x20)
//printf("Block: %x\n",Block);

	//Draw the block..
	for(i=0;i<12;i++)
	{
		Block[i]^=flag;

//if(val!=0x20)
//printf("Block[%d]: %x\n",i,Block[i]);

		cpos = 0x80;
		for(j=0;j<8;j++)
		{
//if(val!=0x20)		
//printf("val %d: %d (%d)\n",j,Block[i]&cpos,cpos);
			if(Block[i]&cpos)
			{
//if(val!=0x20)			
//printf("1");
				colour = colourindex;
			}
			else
			{
//if(val!=0x20)			
//printf("0");
				colour = bgcolourindex;
			}

//			colour = rand()%0xffff;
//			colour = xGreen.pixel;

			XSetForeground(display,gc,colour);			
			XFillRectangle(display,window,gc,screenX,screenY,2,2);

			cpos >>= 1;
			screenX += 2;
		}//for
//if(val!=0x20)		
//printf("\n");		
		screenX-=16;
		screenY+=2;
	}//for
	XFlush(display);

//if(val!=0x20)
//printf("\n");
//if(val!=0x20)
//getchar();
	
}//UpdateDisplay

void SysDep6847::UpdateChip(unsigned tt8bits val)
{
	XSetForeground(display,gc,xGreen->pixel);
	XSetBackground(display,gc,xBlack->pixel);

	VideoMode = SG4;
	bgColour = Black;
	fgColour = Green;

//	printf("Updated chip with %x\n",val);
}//UpdateChip

void SysDep6847::Quit(void)
{
	XDestroyWindow(display,window);
	XCloseDisplay(display);
}//Quit

#undef FIX
