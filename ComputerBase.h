#ifndef __COMPUTERBASE_H
#define __COMPUTERBASE_H

//#define SHOWOPS

//define the generic computer class.. all virtual..

//define data types
//NOTE: This'll need to be changed for other systems
#define tt8bits char
#define tt16bits short
#define tt32bits int

class ComputerBase
{
  public:
    virtual inline unsigned tt8bits FetchMemory(unsigned tt16bits) = 0;
    virtual inline void SetMemory(unsigned tt16bits, unsigned tt8bits) = 0;
    virtual inline void SetRam(unsigned tt16bits, unsigned tt8bits) = 0;
    
	virtual inline void HandleInvalidOpCode(unsigned tt8bits) = 0;
	
    virtual void ShowRegisters(void) = 0;
	


};//ComputerBase class

#endif
