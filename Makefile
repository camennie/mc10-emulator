CC = g++
CFLAGS = -I /usr/X11R6/include -g -O9
LFLAGS = -lX11 -L/usr/X11R6/lib

all: clean mc10emu TokenizeListing

clean:
	rm -f TokenizeListing mc10emu mc10emu.monitor *.o

OBJECTS = 	ReadCassette.o \
		WriteCassette.o \
		CoCoTokenConvert.o \
		6803.o \
		MC10.o \
		mc10init.o \
		6847X11.o 

TokenizeListing: TokenizeListing.o
	$(CC) TokenizeListing.o -o TokenizeListing $(CFLAGS) $(LFLAGS)

TokenizeListing.o: TokenizeListing.cc
	$(CC) -c $< $(CFLAGS)

mc10emu: $(OBJECTS)
	$(CC) $(OBJECTS) -o mc10emu $(CFLAGS) $(LFLAGS); rm -f *.o

mc10emu.monitor:
	$(CC) -DMONITOR mc10init.cc -o mc10emu.monitor ; rm -f *.o

mc10init.o: mc10init.cc
	$(CC) -c $< $(CFLAGS)

ReadCassette.o: ReadCassette.cc
	$(CC) -c $< $(CFLAGS)

WriteCassette.o: WriteCassette.cc
	$(CC) -c $< $(CFLAGS)

CoCoTokenConvert.o: CoCoTokenConvert.cc
	$(CC) -c $< $(CFLAGS)

MC10.o:	MC10.cc
	$(CC) -c $< $(CFLAGS)

6803.o: 6803.cc
	$(CC) -c $< $(CFLAGS)

6847X11.o: 6847X11.cc
	$(CC) -c $< $(CFLAGS)
