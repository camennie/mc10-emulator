#include<stdio.h>
#include<ctype.h>
#include<string.h>

#include "ComputerBase.h"
#include "6803.h"
#include "6803ops.h"
#include "MC10.h"

#define HDMP  200
#define EMAX 7500
//char reg[2];  //accumulator A and B
//short D = (short *)&reg[0]; //A and B combined
//char CCR; //condition code register
//short PC; //program counter
//short S; //stack pointer
//short X; //index register

ComputerBase *Computer;

struct hstruct {
 short last_PC;
 short PC;
 short D;
 short X;
 short S;
 char CC;
} history[HDMP];

int history_ctr=0;
int ShowHist(struct hstruct *h, MC10base *MC10);

int main(int argc,char *argv[])
{
  unsigned short memloc;
  int z,dump=0,hsize=10;
  unsigned char opcode;
  FILE *in;
  char tmpline[100];
  MC10base *MC10 = new MC10base();
  long ecount=0;
    
  Computer = (ComputerBase *)MC10;

  MC10->init();
    
  //load up ROM:
  in=fopen("mc10.rom","rb");
  if(!in)
  {
    printf("Couldn't open rom!\n");
    printf("Exit 10\n");
    exit(1);
  }//if

  //load rom into memory..
  for(z=0;z<8192;z++)
  {
    opcode = fgetc(in);
    MC10->ROM[z]=opcode;
  }//for
            
  fclose(in);
  
  //main loop
  MC10->OpCodes[0x00]->Program_Counter = (MC10->FetchMemory(0xfffe) << 8) 
                                          + MC10->FetchMemory(0xffff);
  
//  printf("starting at address: %x (%ld)\n\n",
//         MC10->OpCodes[0x00]->Program_Counter,
//         MC10->OpCodes[0x00]->Program_Counter);  
  
  //No need to start this initially now that debugging is activated with a keypress
  //MC10->ShowRegisters();
  MC10->DebugPoint=0;
  MC10->Debug=0;
  MC10->WatchPoint=0xffff;
  MC10->WatchValue=0xff;


  //main program loop  

  //Initialize graphics
  MC10->x6847Chip->Init();

  for(;;)
  {
	//Check for keypresses

	ecount++;

	if(!(ecount%=EMAX))
	  do
	  {
		MC10->CheckKey();
		if (!MC10->x6847Chip->ResetButton)
  		  MC10->OpCodes[0x00]->Program_Counter 
  		    = (MC10->FetchMemory(0xfffe) << 8) 
              + MC10->FetchMemory(0xffff);
	  } while (!MC10->x6847Chip->ResetButton);

    if(MC10->OpCodes[0x00]->Program_Counter==MC10->DebugPoint)
       MC10->Debug=1;

	MC10->last_PC = MC10->OpCodes[0x00]->Program_Counter;
    opcode = MC10->FetchOpCode();
            
    if(MC10->OpCodes[opcode]==NULL)
    {
      printf("\n\nUnknown opcode: %x!\n",opcode);
      printf("Exit 11\n");
      exit(1);
    }//if

    MC10->OpCodes[opcode]->operation();
    
	history[history_ctr].last_PC = MC10->last_PC;
	history[history_ctr].PC = MC10->OpCodes[0x00]->Program_Counter;
	history[history_ctr].D  = *MC10->OpCodes[0x00]->Accumulator_D;
	history[history_ctr].X  = MC10->OpCodes[0x00]->Index_Pointer;
	history[history_ctr].S  = MC10->OpCodes[0x00]->Stack_Pointer;
	history[history_ctr].CC = (MC10->OpCodes[0x00])->FlagsToVariable();
	history_ctr++;
    history_ctr%=HDMP;

	if (MC10->FetchMemory(MC10->WatchPoint)==MC10->WatchValue)
	  MC10->Debug=1;

	if (MC10->Debug)
	  ShowHist(&history[(history_ctr+HDMP-1)%HDMP],MC10);

    while (MC10->Debug)
    {printf("Command? ");
     gets(tmpline);
     if (sscanf(tmpline,"j %hX",&memloc)>0)
     {dump=0;
      printf("Jumping to %x\n",memloc);
      MC10->OpCodes[0x00]->Program_Counter = memloc;
      break;
     }
     else if (sscanf(tmpline,"w %x %x",
                     &(MC10->WatchPoint),
                     &(MC10->WatchValue))==2)
     {dump=0;
//    MC10->Debug=0;
      printf("Watch until %04X is %02X\n",
             MC10->WatchPoint,
             MC10->WatchValue);
     }
     else if (sscanf(tmpline,"b %X",&(MC10->DebugPoint))>0)
     {dump=0;
//    MC10->Debug=0;
      printf("Debug point changed to %x\n",MC10->DebugPoint);
     }
     else if (!strcmp(tmpline,"s") || !strlen(tmpline) && !dump)
	 {dump=0;
	  break;
	 }
     else if (!strcmp(tmpline,"c"))
	 {dump=0;
	  MC10->Debug=0;
	  break;
	 }
	 else if (sscanf(tmpline,"h %i",&hsize)>0)
	  printf("History length set to %i\n",
	         hsize = hsize > HDMP ? HDMP :
	                 hsize < 1    ? 1    :
	                                hsize
            );
     else if (!strcmp(tmpline,"h"))
	 {int i;
	  dump=0;
	  for (i=0;i<hsize;i++)
	  {printf("%+3i ",i-hsize+1);
	   ShowHist(&history[(history_ctr+i-hsize)%HDMP],MC10);
	 }}
     else if(!strcmp(tmpline,"n"))
     {dump=0;
      if (MC10->OpCodes[opcode]->Mode == Relative || opcode==0xBD)
      {MC10->DebugPoint = ( MC10->last_PC + 2 + (opcode==0xBD)) & 0xffff;
       MC10->Debug=0;
       printf("Debug point changed to %x\n",MC10->DebugPoint);
      }
      break;
     }
	 else if (sscanf(tmpline,"d %hX",&memloc)>0 || dump && !strlen(tmpline))
	 {int i;
	  dump=1;
	  memloc &= 0xfff0;
	  printf("%04X: ",memloc);
      do
	  {if (MC10->isLegalMemory(memloc))
	    printf("%02X",MC10->FetchMemory(memloc));
       else
        printf("--");
       if (MC10->isLegalMemory(++memloc))
	    printf("%02X ",MC10->FetchMemory(memloc));
       else
        printf("-- ");
      } while (++memloc&0xf);
	  printf("\n");
     }
     else 
     {printf("j XXXX    -- jump to address\n");
      printf("b XXXX    -- set break to address\n");
      printf("w XXXX YY -- watch and break when XXXX holds YY\n");
      printf("n         -- next (break after JSR or Bxx)\n");
      printf("h N       -- set history length to N (max %i)\n",HDMP);
      printf("h         -- history\n");
      printf("s         -- step\n");
      printf("c         -- continue\n");
      printf("d XXXX    -- dump location\n");
      printf("[blank]   -- continue step or dump\n");
     }
    }
    
  }//main for loop
  
}//main

int ShowHist(struct hstruct *h,MC10base *MC10)
{
	 MC10->ShowInstruction(h->last_PC);

     printf("PC:%04hX D:%04hX X:%04hX S:%04hX CC: ",
             h->PC,   h->D,   h->X,   h->S);
     putchar(h->CC & 0x20 ? 'H' : '.');
     putchar(h->CC & 0x10 ? 'I' : '.');
     putchar(h->CC & 0x08 ? 'N' : '.');
     putchar(h->CC & 0x04 ? 'Z' : '.');
     putchar(h->CC & 0x02 ? 'V' : '.');
     putchar(h->CC & 0x01 ? 'C' : '.');
     putchar('\n');
}
