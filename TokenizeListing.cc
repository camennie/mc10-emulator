#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<unistd.h>
#include<fstream.h>

//filetypes
#define BASIC 0x00
#define DATA 0x01
#define ML 0x02

//datatypes
#define BINARY 0x00
#define ASCII 0xff

//gap types
#define CONTINUOUS 0x00
#define GAPS 0xff

unsigned char Buffer[0xffff];
int size=0;

void WriteCassette(FILE *, char *);
void Tokenize(char *line);
int Convert(char *line, char *output);
unsigned char IsSymbol(unsigned char inc);
unsigned char IsWord(char *word);

unsigned char IsSymbol(unsigned char inc)
{
	const int num=35;
	char symbols[num]={'+','-','*','/','^','>','=','<',' ','"','@','\0','(','$',
						'1','2','3','4','5','6','7','8','9','0',
						')','!','"','#','%','&','\'',':',';',',','.'};
	unsigned char tokens[num]={0xa7,0xa8,0xa9,0xaa,0xab,0xae,0xaf,0xb0,' ','"','@','\0','(','$',
								'1','2','3','4','5','6','7','8','9','0',
								')','!','"','#','%','&','\'',':',';',',','.'};
	int x;

	//NOTE: we add a few symbols to help with the recognition of keywords
	//we need this because we're not using a FSM to lexically analyse the lines.
	//The only catch is that it we allow for some lexical errors to occur in the conversion
	//but basic will quickly catch them..


	for(x=0;x<num;x++)
		if(inc==symbols[x])
			return tokens[x];
	
	//we should never have to represent a token as 1 here..
	//I'd like to use null as the catch-all, but that's already in the
	//list of symbols
	return 1;
}//IsSymbol

unsigned char IsWord(char *word)
{
	const int num=65;
	const char *words[num]={"FOR","GOTO","GOSUB","REM","IF","DATA","PRINT","ON","INPUT","END","NEXT","DIM",
						"READ","LET","RUN","RESTORE","RETURN","STOP","POKE","CONT","LIST","CLEAR","NEW",
						"CLOAD","CSAVE","LLIST","LPRINT","SET","RESET","CLS","SOUND","EXEC","SKIPF","TAB",
						"TO","THEN","NOT","STEP","OFF","AND","OR","SGN","INT","ABS","USR","RND","SQR",
						"LOG","EXP","SIN","COS","TAN","PEEK","LEN","STR","VAR","ASC","CHR","LEFT","RIGHT",
						"MID","POINT","VARPTR","INKEY","MEM"};
	char tokens[num]={0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,
						0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,
						0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,
						0xAC,0xAD,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,
						0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8};
	int x;

	if( (word==NULL)||(strlen(word)==0))
		return 0;

	for(x=0;x<num;x++)
		if(!strcmp(word,words[x]))
			return tokens[x];

	return 0;
}//IsWord

int Convert(char *line, char *output)
{
	int LineNumber,x=0,y,tokenPos=0,tmpInt;
	char tmpString[501], inchar[2];
	unsigned char symbol;

	inchar[1]='\0';

	//first, let's filter out the line number
	while(!isspace(line[x++]));
	line[x-1]='\0';

	//get the line number and verify it is a number
	strcpy(tmpString,line);
	for(tmpInt=0;tmpInt<strlen(tmpString);tmpInt++)
		if(!isdigit(line[tmpInt]))
			return -1;

	LineNumber=atoi(tmpString);

	//remove leading and trailing whitespace
	Tokenize(line+x);

	//now, traverse the line, adding tokens when appropriate
	tmpString[0]='\0';
	output[0]='\0';
	tmpInt=x;
	
	//NOTE: the <= strlen(line) ensures we get the trailing 0 and flush the buffer
	for(x=tmpInt;x<=strlen(line+tmpInt)+tmpInt;x++)
	{
		//check to see if the current character is a symbol
		//if not, add the character to the current buffer
		//if so, see if the buffer itself is a token, otherwise
		//just add the buffer content to the output, followed by
		//the symbol's token
		if((symbol=IsSymbol(line[x]))!=1)
		{
			//add tokenized keyword or string to the output
			if(inchar[0]=IsWord(tmpString))
			{
				//another hack.. if the line has been remmed, the rest of it is not tokenized
				strcat(output,inchar);
				if(!strcmp(tmpString,"REM"))
				{
					for(y=x;y<=strlen(line+tmpInt)+tmpInt;y++)
					{
						if(islower(line[y]))
							inchar[0]=toupper(line[y]);
						else
							inchar[0]=tolower(line[y]);

						strcat(output,inchar);
					}//for
//					strcat(output,line+x);
					return LineNumber;
				}//if
			}
			else
				strcat(output,tmpString);


			//here's the bandaid fix for some problems.. if the keyword includes the symbol
			//we obviously don't want to include the symbol twice
			if(!( (inchar[0]) && (	(!strcmp(tmpString,"TAB"))||
									(!strcmp(tmpString,"STR"))||
									(!strcmp(tmpString,"CHR"))||
									(!strcmp(tmpString,"LEFT"))||
									(!strcmp(tmpString,"RIGHT"))||
									(!strcmp(tmpString,"MID"))||
									(!strcmp(tmpString,"INKEY"))
								 ))
				)
				{
					//add symbol
					inchar[0]=symbol;
					strcat(output,inchar);
				}//if

			//and one more hack.. if the symbol as a " we don't tokenize anything withing
			//the string quote, so we want to just dump stuff out until we find the closing "
			if(symbol=='"')
			{
				while( (line[++x]!='"')&&(x<=strlen(line+tmpInt)+tmpInt) )
				{
					if(islower(line[x]))
						inchar[0]=toupper(line[x]);
					else
						inchar[0]=tolower(line[x]);
					strcat(output,inchar);
				}//while

				inchar[0]=line[x];
				strcat(output,inchar);

				//reset inchar
//				inchar[0]=IsWord(tmpString);
			}//if


			//empty buffer
			tmpString[0]='\0';
		}
		else
		{
			//add the current character to the buffer
			if(islower(line[x]))
				inchar[0]=toupper(line[x]);
			else
				inchar[0]=tolower(line[x]);
			strcat(tmpString,inchar);
		}//if
	}//for
	
	return LineNumber;
}//Convert

void Tokenize(char *line)
{
	int x,cnt=0;

	//remove leading whitespace
	x=0;
	while( (line[x]!='\0')&&(isspace(line[x++])) )
		cnt++;

	for(x=cnt;x<=strlen(line);x++)
		line[x-cnt]=line[x];

	//remove trailing whitespace
	x=strlen(line)-1;
	while(isspace(line[x]))
		line[x--]='\0';

}//Tokenize

int main(int argc, char **argv)
{
	ifstream inFile;
	FILE *outfile;
	char *line = (char *)malloc(500);
	char *output = (char *)malloc(500);
	char *progname = (char *)malloc(500);
	int LineNumber,MemLocation=0x4346,x;
	unsigned char tmp, tmp2;

	if(argc<4)
	{
		printf("Usage: %s <source> <dest> <prog name>\n",argv[0]);
		return 0;
	}//if

	inFile.open(argv[1]);
	outfile=fopen(argv[2],"ab");

	if(!inFile)
	{
		printf("Couldn't open '%s' for reading\n");
		return 0;
	}//if

	if(!outfile)
	{
		printf("Couldn't open '%s' for writing\n");
		return 0;
	}//if

	for(x=0;x<8;x++)
		progname[x]=toupper(argv[3][x]);
	progname[9]='\0';

	printf("Reading source '%s', writing to '%s' with program '%s'\n",argv[1],argv[2],progname);

	while(!inFile.eof())
	{
		inFile.getline(line,499);

		//tokenize line (remove leading and trailing whitespace)
		Tokenize(line);

		//convert line to tokens
		//(with the line number returned)
		if(strlen(line)<2)
			continue;
		
		LineNumber=Convert(line,output);

		//check to see if we have a valid line number
		if(LineNumber<0)
		{
			printf("Invalid line number for line '%s', skipping\n",line);
			continue;
		}//if

		//don't bother with the line if it's empty
		if(strlen(output)==0)
			continue;

		//write out line header
		//NOTE: +5 because we need to include the 2 header words and trailing 0
		MemLocation+=strlen(output)+5;
		tmp=(MemLocation>>8)&0xff;
		tmp2=MemLocation&0xff;

		Buffer[size++]=tmp;
		Buffer[size++]=tmp2;
		
		tmp=(LineNumber>>8)&0xff;
		tmp2=LineNumber&0xff;

		Buffer[size++]=tmp;
		Buffer[size++]=tmp2;
	
		//write out tokenized line
		//NOTE: we do strlen(output)+1 so that we include the null as well
		for(x=0;x<strlen(output)+1;x++)
			Buffer[size++]=output[x];
		
	}//while

	//end the file with 0x0000
	tmp=0;
	Buffer[size++]=tmp;
	Buffer[size++]=tmp;

	inFile.close();

	//now flush out the buffer into a cassette format..
	WriteCassette(outfile,progname);

	fclose(outfile);

	return 0;
}//main

void WriteCassette(FILE *out, char *filename)
{
	int x, cnt, pos=0;
	char outfile[50];
	unsigned char tmpc,checksum;
	unsigned char blocktype, datalength, filetype=BASIC, datatype=BINARY, gapflag=CONTINUOUS;
	unsigned short startAddress=0x0000;
	unsigned short loadAddress=0x0000;
	unsigned char tmpBuffer[255];

	//write out leader
	tmpc=0x55;
	for(x=0;x<128;x++)
		fwrite(&tmpc,1,1,out);

	//write filename block
	blocktype=0x00;

	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	tmpc=0x3c;
	fwrite(&tmpc,1,1,out);

	fwrite(&blocktype,1,1,out);
	datalength=0x0f;
	fwrite(&datalength,1,1,out);

	checksum=datalength;	
	checksum+=blocktype;

	for(x=0;x<8;x++)
		tmpBuffer[x]=filename[x];
	
	tmpBuffer[8]=filetype;
	tmpBuffer[9]=datatype;
	tmpBuffer[10]=gapflag;
	tmpBuffer[11]=startAddress<<8;
	tmpBuffer[12]=startAddress&0xff;
	tmpBuffer[13]=loadAddress<<8;
	tmpBuffer[14]=loadAddress&0xff;
	
	for(x=0;x<15;x++)
	{
		fwrite(&tmpBuffer[x],1,1,out);
		checksum+=tmpBuffer[x];
	}//for

	fwrite(&checksum,1,1,out);
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	
	//write out second leader
	tmpc=0x55;
	for(x=0;x<128;x++)
		fwrite(&tmpc,1,1,out);

	//write out data blocks
	cnt=0;
	while(pos<size)
	{
		tmpBuffer[cnt++]=Buffer[pos++];

		if(cnt==255)
		{
			cnt=0;
			
			//flush out buffer
			tmpc=0x55;
			fwrite(&tmpc,1,1,out);
			tmpc=0x3c;
			fwrite(&tmpc,1,1,out);
			tmpc=0x01;
			fwrite(&tmpc,1,1,out);
			checksum=0x01;
			tmpc=0xff;
			fwrite(&tmpc,1,1,out);
			checksum+=0xff;
			for(x=0;x<255;x++)
			{
				fwrite(&tmpBuffer[x],1,1,out);
				checksum+=tmpBuffer[x];
			}//for
			fwrite(&checksum,1,1,out);
			tmpc=0x55;
			fwrite(&tmpc,1,1,out);
		}//if
	}//if

	//flush out buffer
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	tmpc=0x3c;
	fwrite(&tmpc,1,1,out);
	tmpc=0x01;
	fwrite(&tmpc,1,1,out);
	checksum=0x01;
	tmpc=cnt;
	fwrite(&tmpc,1,1,out);
	checksum+=cnt;
	for(x=0;x<cnt;x++)
	{
		fwrite(&tmpBuffer[x],1,1,out);
		checksum+=tmpBuffer[x];
	}//for
	fwrite(&checksum,1,1,out);
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);

	//write out EOF blocks
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	tmpc=0x3c;
	fwrite(&tmpc,1,1,out);
	tmpc=0xff;
	fwrite(&tmpc,1,1,out);
	tmpc=0x00;
	fwrite(&tmpc,1,1,out);
	tmpc=0xff;
	fwrite(&tmpc,1,1,out);
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	
}//WriteCassette




